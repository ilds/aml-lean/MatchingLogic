// Lean compiler output
// Module: MatchingLogic.Definedness
// Imports: Init MatchingLogic.Pattern MatchingLogic.Proof
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
LEAN_EXPORT lean_object* l_ML_memberVar___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__14;
LEAN_EXPORT lean_object* l_ML_equalSelf___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__5;
LEAN_EXPORT lean_object* l_ML_ctxImplDefined___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__2;
uint8_t l_Lean_Syntax_matchesIdent(lean_object*, lean_object*);
lean_object* l___private_Lean_Expr_0__Lean_Expr_getAppNumArgsAux(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1;
static lean_object* l_ML_term_u230a___u230b___closed__7;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__1;
static lean_object* l_ML_delabDefined___closed__3;
LEAN_EXPORT lean_object* l_ML_memberElim(lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_PrettyPrinter_Delaborator_SubExpr_getExpr___at_Lean_PrettyPrinter_Delaborator_getExprKind___spec__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23;
lean_object* l_ML_Proof_conjIntroRule___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28;
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__6;
static lean_object* l_ML_term___u2286_u2286_____closed__4;
static lean_object* l_ML_term_u2308___u2309___closed__15;
LEAN_EXPORT lean_object* l_ML_member___rarg(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Proof_negConjAsImpl___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2;
static lean_object* l_ML_term___u2286_u2286_____closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3;
lean_object* l_ML_Proof_weakeningDisj___rarg(lean_object*, lean_object*);
lean_object* l_ML_AppContext_evars___rarg(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3;
lean_object* l_ML_Proof_doubleNegCtx___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12;
static lean_object* l_ML_term___u2208_u2208_____closed__1;
static lean_object* l_ML_term_u2308___u2309___closed__10;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8;
lean_object* l_Lean_replaceRef(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2;
LEAN_EXPORT lean_object* l_ML_memberDistribCDisjRight___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_definedness_x27___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT uint8_t l_ML_isApplicationOfDefinedSymbolToPattern(lean_object*);
static lean_object* l_ML_term___u2261_____closed__6;
LEAN_EXPORT lean_object* l_ML_memberDistribCDisjLeft(lean_object*);
lean_object* l_ML_Proof_extraPremise___rarg(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Proof_univQuan___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
LEAN_EXPORT lean_object* l_ML_definedness(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7;
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19;
uint8_t l_Lean_Syntax_isOfKind(lean_object*, lean_object*);
static lean_object* l_ML_term___u2286_u2286_____closed__1;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Proof_equivSelf___rarg(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3;
LEAN_EXPORT lean_object* l_ML_instHasDefinedDefinedness;
static lean_object* l_ML_term___u2261_____closed__1;
lean_object* l_ML_Pattern_negation___rarg(lean_object*);
static lean_object* l_ML_delabDefined___closed__6;
static lean_object* l_ML_term_u230a___u230b___closed__3;
static lean_object* l_ML_delabDefined___closed__4;
LEAN_EXPORT lean_object* l_ML_memberDistribCDisjRight(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_delabDefined___closed__2;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1___boxed(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1(lean_object*, lean_object*, lean_object*);
lean_object* lean_sorry(uint8_t);
LEAN_EXPORT lean_object* l_ML_definedness_x27(lean_object*);
LEAN_EXPORT lean_object* l_ML_equalSelf(lean_object*);
lean_object* l_Lean_Name_mkStr3(lean_object*, lean_object*, lean_object*);
lean_object* l_List_appendTR___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_memberVar(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_equivImplEqual___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31;
LEAN_EXPORT lean_object* l_ML_memberDistribCDisjLeft___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Proof_implSelf___rarg(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15;
lean_object* l_ML_Proof_syllogism___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_memberExist___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_delabDefined___closed__7;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7;
lean_object* l_ML_Pattern_universal___rarg(lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__12;
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_term___u2286_u2286__;
static lean_object* l_ML_term_u2308___u2309___closed__9;
lean_object* l_Lean_SourceInfo_fromRef(lean_object*, uint8_t);
static lean_object* l_ML_delabDefined___closed__1;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
LEAN_EXPORT lean_object* l_ML_implDefined___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32;
lean_object* l_ML_Proof_propagationExist___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_auxTauto(lean_object*, lean_object*);
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10;
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__4;
static lean_object* l_ML_term_u2308___u2309___closed__8;
static lean_object* l_ML_term_u230a___u230b___closed__1;
static lean_object* l_ML_term_u2308___u2309___closed__6;
LEAN_EXPORT lean_object* l_ML_memberNegRight___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_ctxImplDefined___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2208_u2208_____closed__2;
LEAN_EXPORT lean_object* l_ML_auxTauto___rarg(lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__13;
lean_object* l_ML_Pattern_equivalence___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___rarg___boxed(lean_object*);
LEAN_EXPORT lean_object* l_ML_memberElim___rarg___boxed(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_term_u2308___u2309;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16;
lean_object* l_ML_Proof_framing___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__11;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1;
lean_object* l_Lean_Syntax_node3(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_PrettyPrinter_Delaborator_delab(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_ML_AppContext_insert___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_equivImplEqual(lean_object*);
lean_object* l_Lean_addMacroScope(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_EVar_getFresh(lean_object*);
LEAN_EXPORT lean_object* l_ML_totalImpl___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__5;
lean_object* l_Lean_Name_str___override(lean_object*, lean_object*);
lean_object* l_ML_Pattern_evars___rarg(lean_object*);
lean_object* l_Lean_Syntax_node2(lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_Lean_Syntax_getArg(lean_object*, lean_object*);
uint8_t l_Lean_Syntax_matchesNull(lean_object*, lean_object*);
static lean_object* l_ML_term___u2261_____closed__5;
LEAN_EXPORT lean_object* l_ML_memberIntro___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_included___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_memberIntro(lean_object*);
lean_object* l_ML_Proof_doubleNegElim___rarg(lean_object*);
LEAN_EXPORT lean_object* l_ML_memberElim___boxed(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2286_u2286_____closed__2;
static lean_object* l_ML_term___u2261_____closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14;
LEAN_EXPORT lean_object* l_ML_memberElim___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_definedness___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_implDefined___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2208_u2208_____closed__6;
LEAN_EXPORT lean_object* l_ML_equal(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9;
LEAN_EXPORT lean_object* l_ML_equal___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4;
LEAN_EXPORT lean_object* l_ML_memberExist(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__7;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6;
LEAN_EXPORT lean_object* l_ML_memberNegLeft(lean_object*);
LEAN_EXPORT lean_object* l_ML_memberNegLeft___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4;
LEAN_EXPORT lean_object* l_ML_AppContext_CtxDefined___rarg(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5;
lean_object* l_ML_Proof_propagationDisjR___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_term___u2208_u2208__;
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_memberVarLeft___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2286_u2286_____closed__5;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5;
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1(lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__4;
LEAN_EXPORT lean_object* l_ML_definedness_x27___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Definedness_toCtorIdx___boxed(lean_object*);
uint8_t lean_nat_dec_eq(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_term___u2261__;
lean_object* l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppArg___at___private_Lean_PrettyPrinter_Delaborator_Builtins_0__Lean_PrettyPrinter_Delaborator_stripParentProjections___spec__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Proof_univGeneralization___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_total(lean_object*);
lean_object* l_Lean_PrettyPrinter_Delaborator_failure___rarg(lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__6;
lean_object* l_Lean_Name_mkStr2(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_delabDefined(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_member(lean_object*);
LEAN_EXPORT lean_object* l_ML_memberNegRight(lean_object*);
static lean_object* l_ML_term_u2308___u2309___closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5;
lean_object* l_Lean_Syntax_node1(lean_object*, lean_object*, lean_object*);
lean_object* l_ML_Pattern_conjunction___rarg(lean_object*, lean_object*);
lean_object* l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppFn___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__1(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_implDefined(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26;
LEAN_EXPORT lean_object* l_ML_AppContext_CtxDefined(lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7;
LEAN_EXPORT lean_object* l_ML_memberNegLeft___rarg(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21;
LEAN_EXPORT lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___boxed(lean_object*);
lean_object* l_ML_Proof_conjIntro___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29;
LEAN_EXPORT lean_object* l_ML_included(lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___boxed(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_Definedness_toCtorIdx(lean_object*);
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__1;
LEAN_EXPORT lean_object* l_ML_memberNegRight___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__9;
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__5;
lean_object* l_ML_Proof_negImplIntro___rarg(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5;
lean_object* l_ML_Proof_pushConjInExist___rarg(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_ctxImplDefined(lean_object*);
lean_object* l_ML_Pattern_disjunction___rarg(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_memberVarLeft(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13;
lean_object* l_Lean_Name_mkStr4(lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term___u2208_u2208_____closed__5;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1;
static lean_object* l_ML_term___u2261_____closed__7;
static lean_object* l_ML_term___u2261_____closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7;
static lean_object* l_ML_term___u2208_u2208_____closed__3;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5;
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___rarg(lean_object*);
static lean_object* l_ML_term___u2261_____closed__2;
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1(lean_object*, lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18;
LEAN_EXPORT lean_object* l_ML_term_u230a___u230b;
static lean_object* l_ML_term___u2208_u2208_____closed__4;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7;
static lean_object* l_ML_delabDefined___closed__5;
static lean_object* l_ML_term___u2286_u2286_____closed__3;
lean_object* l_String_toSubstring_x27(lean_object*);
lean_object* l_ML_Proof_propagationDisj___rarg(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_totalImpl(lean_object*);
LEAN_EXPORT lean_object* l_ML_totalImpl___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_memberIntro___rarg___boxed(lean_object*, lean_object*, lean_object*, lean_object*, lean_object*, lean_object*);
static lean_object* l_ML_term_u230a___u230b___closed__8;
static lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___closed__2;
LEAN_EXPORT lean_object* l_ML_total___rarg(lean_object*, lean_object*);
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20;
static lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2;
LEAN_EXPORT lean_object* l_ML_Definedness_toCtorIdx(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_unsigned_to_nat(0u);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_toCtorIdx___boxed(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = l_ML_Definedness_toCtorIdx(x_1);
lean_dec(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___rarg(lean_object* x_1) {
_start:
{
lean_inc(x_1);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = lean_alloc_closure((void*)(l_ML_Definedness_noConfusion___rarg___boxed), 1, 0);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___rarg___boxed(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = l_ML_Definedness_noConfusion___rarg(x_1);
lean_dec(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_Definedness_noConfusion___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_Definedness_noConfusion(x_1, x_2, x_3, x_4);
lean_dec(x_3);
lean_dec(x_2);
return x_5;
}
}
static lean_object* _init_l_ML_instHasDefinedDefinedness() {
_start:
{
lean_object* x_1; 
x_1 = lean_box(0);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("ML", 2, 2);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("term⌈_⌉", 11, 7);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term_u2308___u2309___closed__2;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("andthen", 7, 7);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_term_u2308___u2309___closed__4;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("⌈", 3, 1);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term_u2308___u2309___closed__6;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__8() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("term", 4, 4);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_term_u2308___u2309___closed__8;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__9;
x_2 = lean_unsigned_to_nat(0u);
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term_u2308___u2309___closed__7;
x_3 = l_ML_term_u2308___u2309___closed__10;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__12() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("⌉", 3, 1);
return x_1;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term_u2308___u2309___closed__12;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__14() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term_u2308___u2309___closed__11;
x_3 = l_ML_term_u2308___u2309___closed__13;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u2308___u2309___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__3;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_term_u2308___u2309___closed__14;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u2308___u2309() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term_u2308___u2309___closed__15;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("Pattern", 7, 7);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("term_⬝_", 9, 7);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("Lean", 4, 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("Parser", 6, 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("Term", 4, 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("paren", 5, 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6;
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("(", 1, 1);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("app", 3, 3);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6;
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10;
x_5 = l_Lean_Name_mkStr4(x_1, x_2, x_3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("Pattern.symbol", 14, 14);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("symbol", 6, 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("null", 4, 4);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("HasDefined.defined", 18, 18);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("HasDefined", 10, 10);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("defined", 7, 7);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25;
x_3 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26;
x_4 = l_Lean_Name_mkStr3(x_1, x_2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked(")", 1, 1);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("⬝", 3, 1);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term_u2308___u2309___closed__3;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; 
x_8 = lean_unsigned_to_nat(1u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
lean_dec(x_1);
x_10 = lean_ctor_get(x_2, 5);
lean_inc(x_10);
x_11 = 0;
x_12 = l_Lean_SourceInfo_fromRef(x_10, x_11);
lean_dec(x_10);
x_13 = lean_ctor_get(x_2, 2);
lean_inc(x_13);
x_14 = lean_ctor_get(x_2, 1);
lean_inc(x_14);
lean_dec(x_2);
x_15 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9;
lean_inc(x_12);
x_16 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_16, 0, x_12);
lean_ctor_set(x_16, 1, x_15);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15;
lean_inc(x_13);
lean_inc(x_14);
x_18 = l_Lean_addMacroScope(x_14, x_17, x_13);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13;
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20;
lean_inc(x_12);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_12);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
x_23 = l_Lean_addMacroScope(x_14, x_22, x_13);
x_24 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24;
x_25 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30;
lean_inc(x_12);
x_26 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_26, 0, x_12);
lean_ctor_set(x_26, 1, x_24);
lean_ctor_set(x_26, 2, x_23);
lean_ctor_set(x_26, 3, x_25);
x_27 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_12);
x_28 = l_Lean_Syntax_node1(x_12, x_27, x_26);
x_29 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_12);
x_30 = l_Lean_Syntax_node2(x_12, x_29, x_21, x_28);
x_31 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31;
lean_inc(x_12);
x_32 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_32, 0, x_12);
lean_ctor_set(x_32, 1, x_31);
x_33 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8;
lean_inc(x_12);
x_34 = l_Lean_Syntax_node3(x_12, x_33, x_16, x_30, x_32);
x_35 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32;
lean_inc(x_12);
x_36 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_36, 0, x_12);
lean_ctor_set(x_36, 1, x_35);
x_37 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3;
x_38 = l_Lean_Syntax_node3(x_12, x_37, x_34, x_36, x_9);
x_39 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_39, 0, x_38);
lean_ctor_set(x_39, 1, x_3);
return x_39;
}
}
}
LEAN_EXPORT lean_object* l_ML_definedness___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
x_3 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_3, 0, x_1);
lean_inc(x_2);
x_4 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_4, 0, x_2);
x_5 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_5, 0, x_3);
lean_ctor_set(x_5, 1, x_4);
x_6 = l_ML_Pattern_universal___rarg(x_2, x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_definedness(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_definedness___rarg), 2, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_total___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
x_3 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_3, 0, x_1);
x_4 = l_ML_Pattern_negation___rarg(x_2);
x_5 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_5, 0, x_3);
lean_ctor_set(x_5, 1, x_4);
x_6 = l_ML_Pattern_negation___rarg(x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_total(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_total___rarg), 2, 0);
return x_2;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("term⌊_⌋", 11, 7);
return x_1;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term_u230a___u230b___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("⌊", 3, 1);
return x_1;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term_u230a___u230b___closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term_u230a___u230b___closed__4;
x_3 = l_ML_term_u2308___u2309___closed__10;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("⌋", 3, 1);
return x_1;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term_u230a___u230b___closed__6;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term_u230a___u230b___closed__5;
x_3 = l_ML_term_u230a___u230b___closed__7;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u230a___u230b___closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u230a___u230b___closed__2;
x_2 = lean_unsigned_to_nat(1024u);
x_3 = l_ML_term_u230a___u230b___closed__8;
x_4 = lean_alloc_ctor(3, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term_u230a___u230b() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term_u230a___u230b___closed__9;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("total", 5, 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term_u230a___u230b___closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; 
x_8 = lean_unsigned_to_nat(1u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
lean_dec(x_1);
x_10 = lean_ctor_get(x_2, 5);
lean_inc(x_10);
x_11 = 0;
x_12 = l_Lean_SourceInfo_fromRef(x_10, x_11);
lean_dec(x_10);
x_13 = lean_ctor_get(x_2, 2);
lean_inc(x_13);
x_14 = lean_ctor_get(x_2, 1);
lean_inc(x_14);
lean_dec(x_2);
x_15 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3;
x_16 = l_Lean_addMacroScope(x_14, x_15, x_13);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2;
x_18 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8;
lean_inc(x_12);
x_19 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_19, 0, x_12);
lean_ctor_set(x_19, 1, x_17);
lean_ctor_set(x_19, 2, x_16);
lean_ctor_set(x_19, 3, x_18);
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_12);
x_21 = l_Lean_Syntax_node1(x_12, x_20, x_9);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
x_23 = l_Lean_Syntax_node2(x_12, x_22, x_19, x_21);
x_24 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_24, 0, x_23);
lean_ctor_set(x_24, 1, x_3);
return x_24;
}
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("ident", 5, 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; uint8_t x_16; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
lean_inc(x_15);
x_16 = l_Lean_Syntax_matchesNull(x_15, x_14);
if (x_16 == 0)
{
lean_object* x_17; lean_object* x_18; 
lean_dec(x_15);
lean_dec(x_9);
x_17 = lean_box(0);
x_18 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_18, 0, x_17);
lean_ctor_set(x_18, 1, x_3);
return x_18;
}
else
{
lean_object* x_19; lean_object* x_20; uint8_t x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; 
x_19 = l_Lean_Syntax_getArg(x_15, x_8);
lean_dec(x_15);
x_20 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_9);
x_21 = 0;
x_22 = l_Lean_SourceInfo_fromRef(x_20, x_21);
lean_dec(x_20);
x_23 = l_ML_term_u230a___u230b___closed__3;
lean_inc(x_22);
x_24 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_24, 0, x_22);
lean_ctor_set(x_24, 1, x_23);
x_25 = l_ML_term_u230a___u230b___closed__6;
lean_inc(x_22);
x_26 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_26, 0, x_22);
lean_ctor_set(x_26, 1, x_25);
x_27 = l_ML_term_u230a___u230b___closed__2;
x_28 = l_Lean_Syntax_node3(x_22, x_27, x_24, x_19, x_26);
x_29 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_29, 0, x_28);
lean_ctor_set(x_29, 1, x_3);
return x_29;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1(x_1, x_2, x_3);
lean_dec(x_2);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_equal___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
x_4 = l_ML_Pattern_equivalence___rarg(x_2, x_3);
x_5 = l_ML_total___rarg(x_1, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_equal(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_equal___rarg), 3, 0);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("term_≡_", 9, 7);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term___u2261_____closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked(" ≡ ", 5, 3);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term___u2261_____closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__9;
x_2 = lean_unsigned_to_nat(51u);
x_3 = lean_alloc_ctor(7, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term___u2261_____closed__4;
x_3 = l_ML_term___u2261_____closed__5;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term___u2261_____closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_term___u2261_____closed__2;
x_2 = lean_unsigned_to_nat(50u);
x_3 = lean_unsigned_to_nat(51u);
x_4 = l_ML_term___u2261_____closed__6;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_term___u2261__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term___u2261_____closed__7;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("equal", 5, 5);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term___u2261_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
lean_dec(x_12);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2;
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_9, x_11);
x_24 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; lean_object* x_19; 
lean_dec(x_15);
lean_dec(x_9);
x_18 = lean_box(0);
x_19 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_19, 0, x_18);
lean_ctor_set(x_19, 1, x_3);
return x_19;
}
else
{
lean_object* x_20; lean_object* x_21; lean_object* x_22; uint8_t x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; 
x_20 = l_Lean_Syntax_getArg(x_15, x_8);
x_21 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_22 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_9);
x_23 = 0;
x_24 = l_Lean_SourceInfo_fromRef(x_22, x_23);
lean_dec(x_22);
x_25 = l_ML_term___u2261_____closed__3;
lean_inc(x_24);
x_26 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_26, 0, x_24);
lean_ctor_set(x_26, 1, x_25);
x_27 = l_ML_term___u2261_____closed__2;
x_28 = l_Lean_Syntax_node3(x_24, x_27, x_20, x_26, x_21);
x_29 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_29, 0, x_28);
lean_ctor_set(x_29, 1, x_3);
return x_29;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__equal__1(x_1, x_2, x_3);
lean_dec(x_2);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_member___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; 
x_4 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_4, 0, x_1);
x_5 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_5, 0, x_2);
x_6 = l_ML_Pattern_conjunction___rarg(x_5, x_3);
x_7 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_7, 0, x_4);
lean_ctor_set(x_7, 1, x_6);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_member(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_member___rarg), 3, 0);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("term_∈∈_", 12, 8);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term___u2208_u2208_____closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked(" ∈∈ ", 8, 4);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term___u2208_u2208_____closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term___u2208_u2208_____closed__4;
x_3 = l_ML_term___u2261_____closed__5;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term___u2208_u2208_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_term___u2208_u2208_____closed__2;
x_2 = lean_unsigned_to_nat(50u);
x_3 = lean_unsigned_to_nat(51u);
x_4 = l_ML_term___u2208_u2208_____closed__5;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_term___u2208_u2208__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term___u2208_u2208_____closed__6;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("member", 6, 6);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term___u2208_u2208_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
lean_dec(x_12);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2;
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_9, x_11);
x_24 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; lean_object* x_19; 
lean_dec(x_15);
lean_dec(x_9);
x_18 = lean_box(0);
x_19 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_19, 0, x_18);
lean_ctor_set(x_19, 1, x_3);
return x_19;
}
else
{
lean_object* x_20; lean_object* x_21; lean_object* x_22; uint8_t x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; 
x_20 = l_Lean_Syntax_getArg(x_15, x_8);
x_21 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_22 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_9);
x_23 = 0;
x_24 = l_Lean_SourceInfo_fromRef(x_22, x_23);
lean_dec(x_22);
x_25 = l_ML_term___u2208_u2208_____closed__3;
lean_inc(x_24);
x_26 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_26, 0, x_24);
lean_ctor_set(x_26, 1, x_25);
x_27 = l_ML_term___u2208_u2208_____closed__2;
x_28 = l_Lean_Syntax_node3(x_24, x_27, x_20, x_26, x_21);
x_29 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_29, 0, x_28);
lean_ctor_set(x_29, 1, x_3);
return x_29;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__member__1(x_1, x_2, x_3);
lean_dec(x_2);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_included___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
x_4 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_4, 0, x_2);
lean_ctor_set(x_4, 1, x_3);
x_5 = l_ML_total___rarg(x_1, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_included(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_included___rarg), 3, 0);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("term_⊆⊆_", 12, 8);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML_term___u2286_u2286_____closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked(" ⊆⊆ ", 8, 4);
return x_1;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_term___u2286_u2286_____closed__3;
x_2 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_1 = l_ML_term_u2308___u2309___closed__5;
x_2 = l_ML_term___u2286_u2286_____closed__4;
x_3 = l_ML_term___u2261_____closed__5;
x_4 = lean_alloc_ctor(2, 3, 0);
lean_ctor_set(x_4, 0, x_1);
lean_ctor_set(x_4, 1, x_2);
lean_ctor_set(x_4, 2, x_3);
return x_4;
}
}
static lean_object* _init_l_ML_term___u2286_u2286_____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; 
x_1 = l_ML_term___u2286_u2286_____closed__2;
x_2 = lean_unsigned_to_nat(50u);
x_3 = lean_unsigned_to_nat(51u);
x_4 = l_ML_term___u2286_u2286_____closed__5;
x_5 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_5, 0, x_1);
lean_ctor_set(x_5, 1, x_2);
lean_ctor_set(x_5, 2, x_3);
lean_ctor_set(x_5, 3, x_4);
return x_5;
}
}
static lean_object* _init_l_ML_term___u2286_u2286__() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_term___u2286_u2286_____closed__6;
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("included", 8, 8);
return x_1;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1;
x_2 = l_String_toSubstring_x27(x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_term_u2308___u2309___closed__1;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4;
x_2 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_2);
lean_ctor_set(x_3, 1, x_1);
return x_3;
}
}
static lean_object* _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5;
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7;
x_3 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML_term___u2286_u2286_____closed__2;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_2);
lean_dec(x_1);
x_6 = lean_box(1);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; uint8_t x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = lean_unsigned_to_nat(2u);
x_11 = l_Lean_Syntax_getArg(x_1, x_10);
lean_dec(x_1);
x_12 = lean_ctor_get(x_2, 5);
lean_inc(x_12);
x_13 = 0;
x_14 = l_Lean_SourceInfo_fromRef(x_12, x_13);
lean_dec(x_12);
x_15 = lean_ctor_get(x_2, 2);
lean_inc(x_15);
x_16 = lean_ctor_get(x_2, 1);
lean_inc(x_16);
lean_dec(x_2);
x_17 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3;
x_18 = l_Lean_addMacroScope(x_16, x_17, x_15);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2;
x_20 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8;
lean_inc(x_14);
x_21 = lean_alloc_ctor(3, 4, 0);
lean_ctor_set(x_21, 0, x_14);
lean_ctor_set(x_21, 1, x_19);
lean_ctor_set(x_21, 2, x_18);
lean_ctor_set(x_21, 3, x_20);
x_22 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22;
lean_inc(x_14);
x_23 = l_Lean_Syntax_node2(x_14, x_22, x_9, x_11);
x_24 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
x_25 = l_Lean_Syntax_node2(x_14, x_24, x_21, x_23);
x_26 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_26, 0, x_25);
lean_ctor_set(x_26, 1, x_3);
return x_26;
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; 
x_4 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_5 = l_Lean_Syntax_isOfKind(x_1, x_4);
if (x_5 == 0)
{
lean_object* x_6; lean_object* x_7; 
lean_dec(x_1);
x_6 = lean_box(0);
x_7 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_7, 0, x_6);
lean_ctor_set(x_7, 1, x_3);
return x_7;
}
else
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; uint8_t x_11; 
x_8 = lean_unsigned_to_nat(0u);
x_9 = l_Lean_Syntax_getArg(x_1, x_8);
x_10 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2;
lean_inc(x_9);
x_11 = l_Lean_Syntax_isOfKind(x_9, x_10);
if (x_11 == 0)
{
lean_object* x_12; lean_object* x_13; 
lean_dec(x_9);
lean_dec(x_1);
x_12 = lean_box(0);
x_13 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_13, 0, x_12);
lean_ctor_set(x_13, 1, x_3);
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; lean_object* x_16; uint8_t x_17; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
x_16 = lean_unsigned_to_nat(2u);
lean_inc(x_15);
x_17 = l_Lean_Syntax_matchesNull(x_15, x_16);
if (x_17 == 0)
{
lean_object* x_18; lean_object* x_19; 
lean_dec(x_15);
lean_dec(x_9);
x_18 = lean_box(0);
x_19 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_19, 0, x_18);
lean_ctor_set(x_19, 1, x_3);
return x_19;
}
else
{
lean_object* x_20; lean_object* x_21; lean_object* x_22; uint8_t x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; 
x_20 = l_Lean_Syntax_getArg(x_15, x_8);
x_21 = l_Lean_Syntax_getArg(x_15, x_14);
lean_dec(x_15);
x_22 = l_Lean_replaceRef(x_9, x_2);
lean_dec(x_9);
x_23 = 0;
x_24 = l_Lean_SourceInfo_fromRef(x_22, x_23);
lean_dec(x_22);
x_25 = l_ML_term___u2286_u2286_____closed__3;
lean_inc(x_24);
x_26 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_26, 0, x_24);
lean_ctor_set(x_26, 1, x_25);
x_27 = l_ML_term___u2286_u2286_____closed__2;
x_28 = l_Lean_Syntax_node3(x_24, x_27, x_20, x_26, x_21);
x_29 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_29, 0, x_28);
lean_ctor_set(x_29, 1, x_3);
return x_29;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML___aux__MatchingLogic__Definedness______unexpand__ML__included__1(x_1, x_2, x_3);
lean_dec(x_2);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_CtxDefined___rarg(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_2 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_2, 0, x_1);
x_3 = lean_box(0);
x_4 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_4, 0, x_2);
lean_ctor_set(x_4, 1, x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_AppContext_CtxDefined(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_AppContext_CtxDefined___rarg), 1, 0);
return x_2;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__1() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__3() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("Mdefined", 8, 8);
return x_1;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__4() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__3;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__5() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("bdefined", 8, 8);
return x_1;
}
}
static lean_object* _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__5;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT uint8_t l_ML_isApplicationOfDefinedSymbolToPattern(lean_object* x_1) {
_start:
{
lean_object* x_2; uint8_t x_3; 
x_2 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11;
lean_inc(x_1);
x_3 = l_Lean_Syntax_isOfKind(x_1, x_2);
if (x_3 == 0)
{
uint8_t x_4; 
lean_dec(x_1);
x_4 = 0;
return x_4;
}
else
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; uint8_t x_8; 
x_5 = lean_unsigned_to_nat(0u);
x_6 = l_Lean_Syntax_getArg(x_1, x_5);
x_7 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16;
x_8 = l_Lean_Syntax_matchesIdent(x_6, x_7);
if (x_8 == 0)
{
lean_object* x_9; uint8_t x_10; 
x_9 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15;
x_10 = l_Lean_Syntax_matchesIdent(x_6, x_9);
if (x_10 == 0)
{
lean_object* x_11; uint8_t x_12; 
x_11 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__1;
x_12 = l_Lean_Syntax_matchesIdent(x_6, x_11);
lean_dec(x_6);
if (x_12 == 0)
{
uint8_t x_13; 
lean_dec(x_1);
x_13 = 0;
return x_13;
}
else
{
lean_object* x_14; lean_object* x_15; uint8_t x_16; 
x_14 = lean_unsigned_to_nat(1u);
x_15 = l_Lean_Syntax_getArg(x_1, x_14);
lean_dec(x_1);
lean_inc(x_15);
x_16 = l_Lean_Syntax_matchesNull(x_15, x_14);
if (x_16 == 0)
{
uint8_t x_17; 
lean_dec(x_15);
x_17 = 0;
return x_17;
}
else
{
lean_object* x_18; lean_object* x_19; uint8_t x_20; 
x_18 = l_Lean_Syntax_getArg(x_15, x_5);
lean_dec(x_15);
x_19 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28;
x_20 = l_Lean_Syntax_matchesIdent(x_18, x_19);
if (x_20 == 0)
{
lean_object* x_21; uint8_t x_22; 
x_21 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
x_22 = l_Lean_Syntax_matchesIdent(x_18, x_21);
if (x_22 == 0)
{
lean_object* x_23; uint8_t x_24; 
x_23 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__2;
x_24 = l_Lean_Syntax_matchesIdent(x_18, x_23);
lean_dec(x_18);
return x_24;
}
else
{
uint8_t x_25; 
lean_dec(x_18);
x_25 = 1;
return x_25;
}
}
else
{
uint8_t x_26; 
lean_dec(x_18);
x_26 = 1;
return x_26;
}
}
}
}
else
{
lean_object* x_27; lean_object* x_28; uint8_t x_29; 
lean_dec(x_6);
x_27 = lean_unsigned_to_nat(1u);
x_28 = l_Lean_Syntax_getArg(x_1, x_27);
lean_dec(x_1);
lean_inc(x_28);
x_29 = l_Lean_Syntax_matchesNull(x_28, x_27);
if (x_29 == 0)
{
uint8_t x_30; 
lean_dec(x_28);
x_30 = 0;
return x_30;
}
else
{
lean_object* x_31; lean_object* x_32; uint8_t x_33; 
x_31 = l_Lean_Syntax_getArg(x_28, x_5);
lean_dec(x_28);
x_32 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28;
x_33 = l_Lean_Syntax_matchesIdent(x_31, x_32);
if (x_33 == 0)
{
lean_object* x_34; uint8_t x_35; 
x_34 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
x_35 = l_Lean_Syntax_matchesIdent(x_31, x_34);
if (x_35 == 0)
{
lean_object* x_36; uint8_t x_37; 
x_36 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__4;
x_37 = l_Lean_Syntax_matchesIdent(x_31, x_36);
lean_dec(x_31);
return x_37;
}
else
{
uint8_t x_38; 
lean_dec(x_31);
x_38 = 1;
return x_38;
}
}
else
{
uint8_t x_39; 
lean_dec(x_31);
x_39 = 1;
return x_39;
}
}
}
}
else
{
lean_object* x_40; lean_object* x_41; uint8_t x_42; 
lean_dec(x_6);
x_40 = lean_unsigned_to_nat(1u);
x_41 = l_Lean_Syntax_getArg(x_1, x_40);
lean_dec(x_1);
lean_inc(x_41);
x_42 = l_Lean_Syntax_matchesNull(x_41, x_40);
if (x_42 == 0)
{
uint8_t x_43; 
lean_dec(x_41);
x_43 = 0;
return x_43;
}
else
{
lean_object* x_44; lean_object* x_45; uint8_t x_46; 
x_44 = l_Lean_Syntax_getArg(x_41, x_5);
lean_dec(x_41);
x_45 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27;
x_46 = l_Lean_Syntax_matchesIdent(x_44, x_45);
if (x_46 == 0)
{
lean_object* x_47; uint8_t x_48; 
x_47 = l_ML_isApplicationOfDefinedSymbolToPattern___closed__6;
x_48 = l_Lean_Syntax_matchesIdent(x_44, x_47);
lean_dec(x_44);
return x_48;
}
else
{
uint8_t x_49; 
lean_dec(x_44);
x_49 = 1;
return x_49;
}
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_isApplicationOfDefinedSymbolToPattern___boxed(lean_object* x_1) {
_start:
{
uint8_t x_2; lean_object* x_3; 
x_2 = l_ML_isApplicationOfDefinedSymbolToPattern(x_1);
x_3 = lean_box(x_2);
return x_3;
}
}
static lean_object* _init_l_ML_delabDefined___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_alloc_closure((void*)(l_Lean_PrettyPrinter_Delaborator_delab), 7, 0);
return x_1;
}
}
static lean_object* _init_l_ML_delabDefined___closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_delabDefined___closed__1;
x_2 = lean_alloc_closure((void*)(l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppArg___at___private_Lean_PrettyPrinter_Delaborator_Builtins_0__Lean_PrettyPrinter_Delaborator_stripParentProjections___spec__1), 8, 1);
lean_closure_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_delabDefined___closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l_ML_delabDefined___closed__2;
x_2 = lean_alloc_closure((void*)(l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppFn___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__1), 8, 1);
lean_closure_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l_ML_delabDefined___closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("choice", 6, 6);
return x_1;
}
}
static lean_object* _init_l_ML_delabDefined___closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l_ML_delabDefined___closed__4;
x_3 = l_Lean_Name_str___override(x_1, x_2);
return x_3;
}
}
static lean_object* _init_l_ML_delabDefined___closed__6() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("Int", 3, 3);
return x_1;
}
}
static lean_object* _init_l_ML_delabDefined___closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l_ML_delabDefined___closed__6;
x_2 = l_ML_term_u2308___u2309___closed__2;
x_3 = l_Lean_Name_mkStr2(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_delabDefined(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; uint8_t x_15; lean_object* x_16; 
lean_inc(x_1);
x_8 = l_Lean_PrettyPrinter_Delaborator_SubExpr_getExpr___at_Lean_PrettyPrinter_Delaborator_getExprKind___spec__1(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
x_9 = lean_ctor_get(x_8, 0);
lean_inc(x_9);
x_10 = lean_ctor_get(x_8, 1);
lean_inc(x_10);
if (lean_is_exclusive(x_8)) {
 lean_ctor_release(x_8, 0);
 lean_ctor_release(x_8, 1);
 x_11 = x_8;
} else {
 lean_dec_ref(x_8);
 x_11 = lean_box(0);
}
x_12 = lean_unsigned_to_nat(0u);
x_13 = l___private_Lean_Expr_0__Lean_Expr_getAppNumArgsAux(x_9, x_12);
lean_dec(x_9);
x_14 = lean_unsigned_to_nat(3u);
x_15 = lean_nat_dec_eq(x_13, x_14);
lean_dec(x_13);
if (x_15 == 0)
{
lean_object* x_87; uint8_t x_88; 
lean_dec(x_11);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_dec(x_1);
x_87 = l_Lean_PrettyPrinter_Delaborator_failure___rarg(x_10);
x_88 = !lean_is_exclusive(x_87);
if (x_88 == 0)
{
return x_87;
}
else
{
lean_object* x_89; lean_object* x_90; lean_object* x_91; 
x_89 = lean_ctor_get(x_87, 0);
x_90 = lean_ctor_get(x_87, 1);
lean_inc(x_90);
lean_inc(x_89);
lean_dec(x_87);
x_91 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_91, 0, x_89);
lean_ctor_set(x_91, 1, x_90);
return x_91;
}
}
else
{
x_16 = x_10;
goto block_86;
}
block_86:
{
lean_object* x_17; lean_object* x_18; 
x_17 = l_ML_delabDefined___closed__3;
lean_inc(x_6);
lean_inc(x_5);
lean_inc(x_4);
lean_inc(x_3);
lean_inc(x_2);
lean_inc(x_1);
x_18 = l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppFn___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__1(x_17, x_1, x_2, x_3, x_4, x_5, x_6, x_16);
if (lean_obj_tag(x_18) == 0)
{
lean_object* x_19; lean_object* x_20; lean_object* x_21; 
x_19 = lean_ctor_get(x_18, 1);
lean_inc(x_19);
lean_dec(x_18);
x_20 = l_ML_delabDefined___closed__2;
lean_inc(x_6);
lean_inc(x_5);
lean_inc(x_4);
lean_inc(x_3);
lean_inc(x_2);
lean_inc(x_1);
x_21 = l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppFn___at_Lean_PrettyPrinter_Delaborator_delabLetFun___spec__1(x_20, x_1, x_2, x_3, x_4, x_5, x_6, x_19);
if (lean_obj_tag(x_21) == 0)
{
lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
x_22 = lean_ctor_get(x_21, 0);
lean_inc(x_22);
x_23 = lean_ctor_get(x_21, 1);
lean_inc(x_23);
lean_dec(x_21);
x_24 = l_ML_delabDefined___closed__1;
lean_inc(x_5);
x_25 = l_Lean_PrettyPrinter_Delaborator_SubExpr_withAppArg___at___private_Lean_PrettyPrinter_Delaborator_Builtins_0__Lean_PrettyPrinter_Delaborator_stripParentProjections___spec__1(x_24, x_1, x_2, x_3, x_4, x_5, x_6, x_23);
if (lean_obj_tag(x_25) == 0)
{
uint8_t x_26; 
x_26 = !lean_is_exclusive(x_25);
if (x_26 == 0)
{
lean_object* x_27; uint8_t x_28; 
x_27 = lean_ctor_get(x_25, 0);
lean_inc(x_22);
x_28 = l_ML_isApplicationOfDefinedSymbolToPattern(x_22);
if (x_28 == 0)
{
lean_object* x_29; uint8_t x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; 
x_29 = lean_ctor_get(x_5, 5);
lean_inc(x_29);
lean_dec(x_5);
x_30 = 0;
x_31 = l_Lean_SourceInfo_fromRef(x_29, x_30);
lean_dec(x_29);
x_32 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32;
lean_inc(x_31);
if (lean_is_scalar(x_11)) {
 x_33 = lean_alloc_ctor(2, 2, 0);
} else {
 x_33 = x_11;
 lean_ctor_set_tag(x_33, 2);
}
lean_ctor_set(x_33, 0, x_31);
lean_ctor_set(x_33, 1, x_32);
x_34 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3;
x_35 = l_Lean_Syntax_node3(x_31, x_34, x_22, x_33, x_27);
lean_ctor_set(x_25, 0, x_35);
return x_25;
}
else
{
lean_object* x_36; uint8_t x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; 
lean_dec(x_22);
x_36 = lean_ctor_get(x_5, 5);
lean_inc(x_36);
lean_dec(x_5);
x_37 = 0;
x_38 = l_Lean_SourceInfo_fromRef(x_36, x_37);
lean_dec(x_36);
x_39 = l_ML_term_u2308___u2309___closed__6;
lean_inc(x_38);
if (lean_is_scalar(x_11)) {
 x_40 = lean_alloc_ctor(2, 2, 0);
} else {
 x_40 = x_11;
 lean_ctor_set_tag(x_40, 2);
}
lean_ctor_set(x_40, 0, x_38);
lean_ctor_set(x_40, 1, x_39);
x_41 = l_ML_term_u2308___u2309___closed__12;
lean_inc(x_38);
x_42 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_42, 0, x_38);
lean_ctor_set(x_42, 1, x_41);
x_43 = l_ML_term_u2308___u2309___closed__3;
lean_inc(x_42);
lean_inc(x_27);
lean_inc(x_40);
lean_inc(x_38);
x_44 = l_Lean_Syntax_node3(x_38, x_43, x_40, x_27, x_42);
x_45 = l_ML_delabDefined___closed__7;
lean_inc(x_38);
x_46 = l_Lean_Syntax_node3(x_38, x_45, x_40, x_27, x_42);
x_47 = l_ML_delabDefined___closed__5;
x_48 = l_Lean_Syntax_node2(x_38, x_47, x_44, x_46);
lean_ctor_set(x_25, 0, x_48);
return x_25;
}
}
else
{
lean_object* x_49; lean_object* x_50; uint8_t x_51; 
x_49 = lean_ctor_get(x_25, 0);
x_50 = lean_ctor_get(x_25, 1);
lean_inc(x_50);
lean_inc(x_49);
lean_dec(x_25);
lean_inc(x_22);
x_51 = l_ML_isApplicationOfDefinedSymbolToPattern(x_22);
if (x_51 == 0)
{
lean_object* x_52; uint8_t x_53; lean_object* x_54; lean_object* x_55; lean_object* x_56; lean_object* x_57; lean_object* x_58; lean_object* x_59; 
x_52 = lean_ctor_get(x_5, 5);
lean_inc(x_52);
lean_dec(x_5);
x_53 = 0;
x_54 = l_Lean_SourceInfo_fromRef(x_52, x_53);
lean_dec(x_52);
x_55 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32;
lean_inc(x_54);
if (lean_is_scalar(x_11)) {
 x_56 = lean_alloc_ctor(2, 2, 0);
} else {
 x_56 = x_11;
 lean_ctor_set_tag(x_56, 2);
}
lean_ctor_set(x_56, 0, x_54);
lean_ctor_set(x_56, 1, x_55);
x_57 = l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3;
x_58 = l_Lean_Syntax_node3(x_54, x_57, x_22, x_56, x_49);
x_59 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_59, 0, x_58);
lean_ctor_set(x_59, 1, x_50);
return x_59;
}
else
{
lean_object* x_60; uint8_t x_61; lean_object* x_62; lean_object* x_63; lean_object* x_64; lean_object* x_65; lean_object* x_66; lean_object* x_67; lean_object* x_68; lean_object* x_69; lean_object* x_70; lean_object* x_71; lean_object* x_72; lean_object* x_73; 
lean_dec(x_22);
x_60 = lean_ctor_get(x_5, 5);
lean_inc(x_60);
lean_dec(x_5);
x_61 = 0;
x_62 = l_Lean_SourceInfo_fromRef(x_60, x_61);
lean_dec(x_60);
x_63 = l_ML_term_u2308___u2309___closed__6;
lean_inc(x_62);
if (lean_is_scalar(x_11)) {
 x_64 = lean_alloc_ctor(2, 2, 0);
} else {
 x_64 = x_11;
 lean_ctor_set_tag(x_64, 2);
}
lean_ctor_set(x_64, 0, x_62);
lean_ctor_set(x_64, 1, x_63);
x_65 = l_ML_term_u2308___u2309___closed__12;
lean_inc(x_62);
x_66 = lean_alloc_ctor(2, 2, 0);
lean_ctor_set(x_66, 0, x_62);
lean_ctor_set(x_66, 1, x_65);
x_67 = l_ML_term_u2308___u2309___closed__3;
lean_inc(x_66);
lean_inc(x_49);
lean_inc(x_64);
lean_inc(x_62);
x_68 = l_Lean_Syntax_node3(x_62, x_67, x_64, x_49, x_66);
x_69 = l_ML_delabDefined___closed__7;
lean_inc(x_62);
x_70 = l_Lean_Syntax_node3(x_62, x_69, x_64, x_49, x_66);
x_71 = l_ML_delabDefined___closed__5;
x_72 = l_Lean_Syntax_node2(x_62, x_71, x_68, x_70);
x_73 = lean_alloc_ctor(0, 2, 0);
lean_ctor_set(x_73, 0, x_72);
lean_ctor_set(x_73, 1, x_50);
return x_73;
}
}
}
else
{
uint8_t x_74; 
lean_dec(x_22);
lean_dec(x_11);
lean_dec(x_5);
x_74 = !lean_is_exclusive(x_25);
if (x_74 == 0)
{
return x_25;
}
else
{
lean_object* x_75; lean_object* x_76; lean_object* x_77; 
x_75 = lean_ctor_get(x_25, 0);
x_76 = lean_ctor_get(x_25, 1);
lean_inc(x_76);
lean_inc(x_75);
lean_dec(x_25);
x_77 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_77, 0, x_75);
lean_ctor_set(x_77, 1, x_76);
return x_77;
}
}
}
else
{
uint8_t x_78; 
lean_dec(x_11);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_dec(x_1);
x_78 = !lean_is_exclusive(x_21);
if (x_78 == 0)
{
return x_21;
}
else
{
lean_object* x_79; lean_object* x_80; lean_object* x_81; 
x_79 = lean_ctor_get(x_21, 0);
x_80 = lean_ctor_get(x_21, 1);
lean_inc(x_80);
lean_inc(x_79);
lean_dec(x_21);
x_81 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_81, 0, x_79);
lean_ctor_set(x_81, 1, x_80);
return x_81;
}
}
}
else
{
uint8_t x_82; 
lean_dec(x_11);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_3);
lean_dec(x_2);
lean_dec(x_1);
x_82 = !lean_is_exclusive(x_18);
if (x_82 == 0)
{
return x_18;
}
else
{
lean_object* x_83; lean_object* x_84; lean_object* x_85; 
x_83 = lean_ctor_get(x_18, 0);
x_84 = lean_ctor_get(x_18, 1);
lean_inc(x_84);
lean_inc(x_83);
lean_dec(x_18);
x_85 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_85, 0, x_83);
lean_ctor_set(x_85, 1, x_84);
return x_85;
}
}
}
}
}
LEAN_EXPORT lean_object* l_ML_equivImplEqual___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; 
x_6 = l_ML_Pattern_equivalence___rarg(x_3, x_4);
x_7 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_7, 0, x_1);
x_8 = lean_box(0);
x_9 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_9, 0, x_7);
lean_ctor_set(x_9, 1, x_8);
x_10 = l_ML_Proof_doubleNegCtx___rarg(x_6, x_9, x_5);
return x_10;
}
}
LEAN_EXPORT lean_object* l_ML_equivImplEqual(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_equivImplEqual___rarg), 5, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_equalSelf___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; lean_object* x_5; 
lean_inc(x_3);
x_4 = l_ML_Proof_equivSelf___rarg(x_3);
lean_inc(x_3);
x_5 = l_ML_equivImplEqual___rarg(x_1, lean_box(0), x_3, x_3, x_4);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_equalSelf(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_equalSelf___rarg), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_definedness_x27___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; 
x_5 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_5, 0, x_1);
lean_inc(x_4);
x_6 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_6, 0, x_4);
x_7 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_7, 0, x_5);
lean_ctor_set(x_7, 1, x_6);
lean_inc(x_7);
lean_inc(x_4);
x_8 = l_ML_Pattern_universal___rarg(x_4, x_7);
lean_inc(x_8);
x_9 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_9, 0, x_8);
lean_inc(x_4);
lean_inc(x_7);
x_10 = l_ML_Proof_univQuan___rarg(x_7, x_4, x_4, lean_box(0));
x_11 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_11, 0, x_8);
lean_ctor_set(x_11, 1, x_7);
lean_ctor_set(x_11, 2, x_9);
lean_ctor_set(x_11, 3, x_10);
return x_11;
}
}
LEAN_EXPORT lean_object* l_ML_definedness_x27(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_definedness_x27___rarg___boxed), 4, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_definedness_x27___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_definedness_x27___rarg(x_1, x_2, x_3, x_4);
lean_dec(x_3);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_memberIntro___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; 
lean_inc(x_5);
x_7 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_7, 0, x_5);
lean_inc(x_7);
lean_inc(x_4);
x_8 = l_ML_Proof_extraPremise___rarg(x_4, x_7, x_6);
lean_inc(x_7);
x_9 = l_ML_Proof_implSelf___rarg(x_7);
lean_inc(x_4);
lean_inc(x_7);
x_10 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_10, 0, x_7);
lean_ctor_set(x_10, 1, x_4);
lean_inc(x_4);
lean_inc(x_7);
x_11 = l_ML_Pattern_conjunction___rarg(x_7, x_4);
lean_inc(x_11);
lean_inc(x_7);
x_12 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_12, 0, x_7);
lean_ctor_set(x_12, 1, x_11);
lean_inc_n(x_7, 2);
x_13 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_13, 0, x_7);
lean_ctor_set(x_13, 1, x_7);
lean_inc(x_12);
lean_inc(x_10);
x_14 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_12);
lean_inc(x_4);
lean_inc_n(x_7, 2);
x_15 = l_ML_Proof_conjIntro___rarg(x_7, x_7, x_4);
x_16 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_16, 0, x_13);
lean_ctor_set(x_16, 1, x_14);
lean_ctor_set(x_16, 2, x_9);
lean_ctor_set(x_16, 3, x_15);
x_17 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_17, 0, x_10);
lean_ctor_set(x_17, 1, x_12);
lean_ctor_set(x_17, 2, x_8);
lean_ctor_set(x_17, 3, x_16);
lean_inc(x_7);
x_18 = l_ML_Pattern_negation___rarg(x_7);
lean_inc(x_4);
x_19 = l_ML_Pattern_negation___rarg(x_4);
x_20 = l_ML_Pattern_disjunction___rarg(x_18, x_19);
x_21 = l_ML_Pattern_negation___rarg(x_20);
lean_inc(x_1);
x_22 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_22, 0, x_1);
x_23 = lean_box(0);
lean_inc(x_22);
x_24 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_24, 0, x_22);
lean_ctor_set(x_24, 1, x_23);
x_25 = l_ML_Proof_framing___rarg(x_7, x_21, x_24, x_17);
lean_dec(x_17);
lean_dec(x_21);
lean_inc(x_5);
lean_inc(x_1);
x_26 = l_ML_definedness_x27___rarg(x_1, lean_box(0), x_3, x_5);
lean_inc(x_22);
x_27 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_27, 0, x_22);
lean_ctor_set(x_27, 1, x_7);
x_28 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_28, 0, x_22);
lean_ctor_set(x_28, 1, x_11);
x_29 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_29, 0, x_27);
lean_ctor_set(x_29, 1, x_28);
lean_ctor_set(x_29, 2, x_26);
lean_ctor_set(x_29, 3, x_25);
lean_inc(x_5);
x_30 = l_ML_member___rarg(x_1, x_5, x_4);
x_31 = l_ML_Proof_univGeneralization___rarg(x_30, x_5, x_29);
return x_31;
}
}
LEAN_EXPORT lean_object* l_ML_memberIntro(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_memberIntro___rarg___boxed), 6, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_memberIntro___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; 
x_7 = l_ML_memberIntro___rarg(x_1, x_2, x_3, x_4, x_5, x_6);
lean_dec(x_3);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_memberElim___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; uint8_t x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; 
lean_inc(x_1);
x_4 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_4, 0, x_1);
x_5 = 0;
x_6 = lean_sorry(x_5);
lean_inc(x_1);
x_7 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_7, 0, x_1);
lean_ctor_set(x_7, 1, x_4);
x_8 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_8, 0, x_1);
x_9 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_9, 0, x_7);
lean_ctor_set(x_9, 1, x_2);
lean_ctor_set(x_9, 2, x_8);
lean_ctor_set(x_9, 3, x_6);
return x_9;
}
}
LEAN_EXPORT lean_object* l_ML_memberElim(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = lean_alloc_closure((void*)(l_ML_memberElim___rarg___boxed), 3, 0);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_memberElim___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML_memberElim___rarg(x_1, x_2, x_3);
lean_dec(x_3);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_memberElim___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l_ML_memberElim(x_1, x_2, x_3);
lean_dec(x_2);
return x_4;
}
}
LEAN_EXPORT lean_object* l_ML_memberVarLeft(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
uint8_t x_7; lean_object* x_8; 
x_7 = 0;
x_8 = lean_sorry(x_7);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_memberVarLeft___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; 
x_7 = l_ML_memberVarLeft(x_1, x_2, x_3, x_4, x_5, x_6);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_memberVar(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
uint8_t x_6; lean_object* x_7; 
x_6 = 0;
x_7 = lean_sorry(x_6);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_memberVar___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; 
x_6 = l_ML_memberVar(x_1, x_2, x_3, x_4, x_5);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_memberNegLeft___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
uint8_t x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; 
x_6 = 0;
x_7 = lean_sorry(x_6);
x_8 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_8, 0, x_1);
x_9 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_9, 0, x_4);
lean_inc(x_3);
x_10 = l_ML_Pattern_negation___rarg(x_3);
lean_inc(x_9);
x_11 = l_ML_Pattern_conjunction___rarg(x_9, x_10);
lean_inc(x_8);
x_12 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_12, 0, x_8);
lean_ctor_set(x_12, 1, x_11);
x_13 = l_ML_Pattern_conjunction___rarg(x_9, x_3);
x_14 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_14, 0, x_8);
lean_ctor_set(x_14, 1, x_13);
lean_inc(x_14);
lean_inc(x_12);
x_15 = l_ML_Pattern_conjunction___rarg(x_12, x_14);
x_16 = l_ML_Pattern_negation___rarg(x_15);
lean_inc(x_14);
x_17 = l_ML_Pattern_negation___rarg(x_14);
lean_inc(x_12);
x_18 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_18, 0, x_12);
lean_ctor_set(x_18, 1, x_17);
x_19 = l_ML_Proof_negConjAsImpl___rarg(x_12, x_14);
x_20 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_20, 0, x_16);
lean_ctor_set(x_20, 1, x_18);
lean_ctor_set(x_20, 2, x_7);
lean_ctor_set(x_20, 3, x_19);
return x_20;
}
}
LEAN_EXPORT lean_object* l_ML_memberNegLeft(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_memberNegLeft___rarg___boxed), 5, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_memberNegLeft___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; 
x_6 = l_ML_memberNegLeft___rarg(x_1, x_2, x_3, x_4, x_5);
lean_dec(x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_memberNegRight___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; 
lean_inc(x_5);
lean_inc(x_1);
x_7 = l_ML_definedness_x27___rarg(x_1, lean_box(0), x_3, x_5);
x_8 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_8, 0, x_5);
lean_inc(x_4);
lean_inc(x_8);
x_9 = l_ML_Pattern_conjunction___rarg(x_8, x_4);
x_10 = l_ML_Pattern_negation___rarg(x_4);
lean_inc(x_10);
lean_inc(x_8);
x_11 = l_ML_Pattern_conjunction___rarg(x_8, x_10);
lean_inc(x_11);
lean_inc(x_9);
x_12 = l_ML_Pattern_disjunction___rarg(x_9, x_11);
lean_inc(x_1);
x_13 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_13, 0, x_1);
lean_inc(x_12);
lean_inc(x_8);
x_14 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_14, 0, x_8);
lean_ctor_set(x_14, 1, x_12);
x_15 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_15, 0, x_14);
lean_inc(x_13);
lean_inc(x_12);
lean_inc(x_8);
x_16 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_16, 0, x_8);
lean_ctor_set(x_16, 1, x_12);
lean_ctor_set(x_16, 2, x_13);
lean_ctor_set(x_16, 3, x_15);
lean_inc(x_8);
lean_inc(x_13);
x_17 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_17, 0, x_13);
lean_ctor_set(x_17, 1, x_8);
lean_inc(x_13);
x_18 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_18, 0, x_13);
lean_ctor_set(x_18, 1, x_12);
x_19 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_19, 0, x_17);
lean_ctor_set(x_19, 1, x_18);
lean_ctor_set(x_19, 2, x_7);
lean_ctor_set(x_19, 3, x_16);
x_20 = l_ML_Pattern_negation___rarg(x_8);
lean_inc(x_10);
lean_inc(x_20);
x_21 = l_ML_Pattern_disjunction___rarg(x_20, x_10);
x_22 = l_ML_Pattern_negation___rarg(x_21);
x_23 = l_ML_Pattern_negation___rarg(x_10);
x_24 = l_ML_Pattern_disjunction___rarg(x_20, x_23);
x_25 = l_ML_Pattern_negation___rarg(x_24);
lean_inc(x_25);
lean_inc(x_22);
x_26 = l_ML_Pattern_disjunction___rarg(x_22, x_25);
x_27 = l_ML_AppContext_CtxDefined___rarg(x_1);
lean_inc(x_27);
x_28 = l_ML_AppContext_insert___rarg(x_26, x_27);
lean_dec(x_26);
lean_inc(x_13);
x_29 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_29, 0, x_13);
lean_ctor_set(x_29, 1, x_9);
x_30 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_30, 0, x_13);
lean_ctor_set(x_30, 1, x_11);
x_31 = l_ML_Pattern_disjunction___rarg(x_29, x_30);
x_32 = l_ML_Proof_propagationDisj___rarg(x_22, x_25, x_27);
x_33 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_33, 0, x_28);
lean_ctor_set(x_33, 1, x_31);
lean_ctor_set(x_33, 2, x_19);
lean_ctor_set(x_33, 3, x_32);
return x_33;
}
}
LEAN_EXPORT lean_object* l_ML_memberNegRight(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_memberNegRight___rarg___boxed), 6, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_memberNegRight___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; 
x_7 = l_ML_memberNegRight___rarg(x_1, x_2, x_3, x_4, x_5, x_6);
lean_dec(x_6);
lean_dec(x_3);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_memberDistribCDisjLeft___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; 
x_6 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_6, 0, x_3);
x_7 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_7, 0, x_4);
x_8 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_8, 0, x_5);
lean_inc(x_8);
lean_inc(x_7);
x_9 = l_ML_Pattern_disjunction___rarg(x_7, x_8);
lean_inc(x_6);
x_10 = l_ML_Pattern_conjunction___rarg(x_6, x_9);
lean_inc(x_6);
x_11 = l_ML_Pattern_conjunction___rarg(x_6, x_7);
x_12 = l_ML_Pattern_conjunction___rarg(x_6, x_8);
lean_inc(x_12);
lean_inc(x_11);
x_13 = l_ML_Pattern_disjunction___rarg(x_11, x_12);
lean_inc(x_13);
lean_inc(x_10);
x_14 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_13);
x_15 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_15, 0, x_14);
lean_inc(x_1);
x_16 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_16, 0, x_1);
lean_inc(x_16);
lean_inc(x_13);
lean_inc(x_10);
x_17 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_17, 0, x_10);
lean_ctor_set(x_17, 1, x_13);
lean_ctor_set(x_17, 2, x_16);
lean_ctor_set(x_17, 3, x_15);
x_18 = l_ML_AppContext_CtxDefined___rarg(x_1);
lean_inc(x_12);
lean_inc(x_11);
x_19 = l_ML_Proof_propagationDisj___rarg(x_11, x_12, x_18);
lean_inc(x_16);
x_20 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_20, 0, x_16);
lean_ctor_set(x_20, 1, x_10);
lean_inc(x_16);
x_21 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_21, 0, x_16);
lean_ctor_set(x_21, 1, x_13);
lean_inc(x_16);
x_22 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_22, 0, x_16);
lean_ctor_set(x_22, 1, x_11);
x_23 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_23, 0, x_16);
lean_ctor_set(x_23, 1, x_12);
x_24 = l_ML_Pattern_disjunction___rarg(x_22, x_23);
x_25 = l_ML_Proof_syllogism___rarg(x_20, x_21, x_24, x_17, x_19);
return x_25;
}
}
LEAN_EXPORT lean_object* l_ML_memberDistribCDisjLeft(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_memberDistribCDisjLeft___rarg), 5, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_memberDistribCDisjRight(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
uint8_t x_7; lean_object* x_8; 
x_7 = 0;
x_8 = lean_sorry(x_7);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_memberDistribCDisjRight___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; 
x_7 = l_ML_memberDistribCDisjRight(x_1, x_2, x_3, x_4, x_5, x_6);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_memberExist(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
uint8_t x_8; lean_object* x_9; 
x_8 = 0;
x_9 = lean_sorry(x_8);
return x_9;
}
}
LEAN_EXPORT lean_object* l_ML_memberExist___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6, lean_object* x_7) {
_start:
{
lean_object* x_8; 
x_8 = l_ML_memberExist(x_1, x_2, x_3, x_4, x_5, x_6, x_7);
lean_dec(x_6);
lean_dec(x_5);
lean_dec(x_4);
lean_dec(x_2);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_auxTauto___rarg(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; 
lean_inc(x_2);
lean_inc(x_1);
x_3 = l_ML_Pattern_disjunction___rarg(x_1, x_2);
lean_inc(x_2);
x_4 = l_ML_Pattern_negation___rarg(x_2);
x_5 = l_ML_Pattern_conjunction___rarg(x_1, x_4);
x_6 = l_ML_Pattern_disjunction___rarg(x_5, x_2);
x_7 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_7, 0, x_3);
lean_ctor_set(x_7, 1, x_6);
x_8 = lean_alloc_ctor(1, 1, 0);
lean_ctor_set(x_8, 0, x_7);
return x_8;
}
}
LEAN_EXPORT lean_object* l_ML_auxTauto(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = lean_alloc_closure((void*)(l_ML_auxTauto___rarg), 2, 0);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; lean_object* x_33; lean_object* x_34; lean_object* x_35; lean_object* x_36; lean_object* x_37; lean_object* x_38; lean_object* x_39; lean_object* x_40; lean_object* x_41; lean_object* x_42; lean_object* x_43; lean_object* x_44; lean_object* x_45; lean_object* x_46; lean_object* x_47; lean_object* x_48; lean_object* x_49; 
lean_inc(x_6);
lean_inc(x_1);
x_7 = l_ML_definedness_x27___rarg(x_1, lean_box(0), x_3, x_6);
lean_inc(x_1);
x_8 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_8, 0, x_1);
lean_inc(x_6);
x_9 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_9, 0, x_6);
lean_inc(x_9);
lean_inc(x_8);
x_10 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_10, 0, x_8);
lean_ctor_set(x_10, 1, x_9);
lean_inc(x_4);
lean_inc(x_8);
x_11 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_11, 0, x_8);
lean_ctor_set(x_11, 1, x_4);
lean_inc(x_11);
lean_inc(x_10);
x_12 = l_ML_Pattern_disjunction___rarg(x_10, x_11);
lean_inc(x_11);
lean_inc(x_10);
x_13 = l_ML_Proof_weakeningDisj___rarg(x_10, x_11);
x_14 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_14, 0, x_10);
lean_ctor_set(x_14, 1, x_12);
lean_ctor_set(x_14, 2, x_7);
lean_ctor_set(x_14, 3, x_13);
x_15 = l_ML_AppContext_CtxDefined___rarg(x_1);
lean_inc(x_15);
x_16 = l_ML_AppContext_insert___rarg(x_9, x_15);
lean_inc(x_15);
x_17 = l_ML_AppContext_insert___rarg(x_4, x_15);
x_18 = l_ML_Pattern_disjunction___rarg(x_16, x_17);
lean_inc(x_4);
lean_inc(x_9);
x_19 = l_ML_Pattern_disjunction___rarg(x_9, x_4);
lean_inc(x_19);
lean_inc(x_8);
x_20 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_20, 0, x_8);
lean_ctor_set(x_20, 1, x_19);
lean_inc(x_15);
lean_inc(x_4);
lean_inc(x_9);
x_21 = l_ML_Proof_propagationDisjR___rarg(x_9, x_4, x_15);
lean_inc(x_20);
x_22 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_22, 0, x_18);
lean_ctor_set(x_22, 1, x_20);
lean_ctor_set(x_22, 2, x_14);
lean_ctor_set(x_22, 3, x_21);
lean_inc(x_4);
lean_inc(x_9);
x_23 = l_ML_auxTauto___rarg(x_9, x_4);
lean_inc(x_4);
x_24 = l_ML_Pattern_negation___rarg(x_4);
lean_inc(x_24);
lean_inc(x_9);
x_25 = l_ML_Pattern_conjunction___rarg(x_9, x_24);
lean_inc(x_4);
lean_inc(x_25);
x_26 = l_ML_Pattern_disjunction___rarg(x_25, x_4);
lean_inc(x_8);
lean_inc(x_26);
x_27 = lean_alloc_ctor(14, 4, 0);
lean_ctor_set(x_27, 0, x_19);
lean_ctor_set(x_27, 1, x_26);
lean_ctor_set(x_27, 2, x_8);
lean_ctor_set(x_27, 3, x_23);
lean_inc(x_8);
x_28 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_28, 0, x_8);
lean_ctor_set(x_28, 1, x_26);
x_29 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_29, 0, x_20);
lean_ctor_set(x_29, 1, x_28);
lean_ctor_set(x_29, 2, x_22);
lean_ctor_set(x_29, 3, x_27);
lean_inc(x_9);
x_30 = l_ML_Pattern_negation___rarg(x_9);
x_31 = l_ML_Pattern_negation___rarg(x_24);
x_32 = l_ML_Pattern_disjunction___rarg(x_30, x_31);
x_33 = l_ML_Pattern_negation___rarg(x_32);
lean_inc(x_4);
lean_inc(x_33);
x_34 = l_ML_Pattern_disjunction___rarg(x_33, x_4);
lean_inc(x_15);
x_35 = l_ML_AppContext_insert___rarg(x_34, x_15);
lean_dec(x_34);
x_36 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_36, 0, x_8);
lean_ctor_set(x_36, 1, x_25);
lean_inc(x_11);
lean_inc(x_36);
x_37 = l_ML_Pattern_disjunction___rarg(x_36, x_11);
lean_inc(x_15);
lean_inc(x_4);
x_38 = l_ML_Proof_propagationDisj___rarg(x_33, x_4, x_15);
x_39 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_39, 0, x_35);
lean_ctor_set(x_39, 1, x_37);
lean_ctor_set(x_39, 2, x_29);
lean_ctor_set(x_39, 3, x_38);
lean_inc(x_4);
lean_inc(x_5);
x_40 = lean_alloc_ctor(6, 4, 0);
lean_ctor_set(x_40, 0, x_5);
lean_ctor_set(x_40, 1, x_15);
lean_ctor_set(x_40, 2, x_6);
lean_ctor_set(x_40, 3, x_4);
x_41 = l_ML_Pattern_conjunction___rarg(x_9, x_4);
x_42 = l_ML_AppContext_insert___rarg(x_41, x_5);
lean_dec(x_41);
lean_inc(x_36);
lean_inc(x_42);
x_43 = l_ML_Pattern_conjunction___rarg(x_42, x_36);
x_44 = l_ML_Pattern_negation___rarg(x_43);
lean_inc(x_36);
x_45 = l_ML_Pattern_negation___rarg(x_36);
lean_inc(x_45);
lean_inc(x_42);
x_46 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_46, 0, x_42);
lean_ctor_set(x_46, 1, x_45);
lean_inc(x_42);
x_47 = l_ML_Proof_negConjAsImpl___rarg(x_42, x_36);
x_48 = lean_alloc_ctor(2, 4, 0);
lean_ctor_set(x_48, 0, x_44);
lean_ctor_set(x_48, 1, x_46);
lean_ctor_set(x_48, 2, x_40);
lean_ctor_set(x_48, 3, x_47);
x_49 = l_ML_Proof_syllogism___rarg(x_42, x_45, x_11, x_48, x_39);
return x_49;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_ctxImplDefinedAux1___rarg___boxed), 6, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefinedAux1___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5, lean_object* x_6) {
_start:
{
lean_object* x_7; 
x_7 = l_ML_ctxImplDefinedAux1___rarg(x_1, x_2, x_3, x_4, x_5, x_6);
lean_dec(x_3);
return x_7;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefined___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; lean_object* x_18; lean_object* x_19; lean_object* x_20; lean_object* x_21; lean_object* x_22; lean_object* x_23; lean_object* x_24; lean_object* x_25; lean_object* x_26; lean_object* x_27; lean_object* x_28; lean_object* x_29; lean_object* x_30; lean_object* x_31; lean_object* x_32; 
x_6 = l_ML_Pattern_evars___rarg(x_4);
x_7 = l_ML_AppContext_evars___rarg(x_5);
x_8 = l_List_appendTR___rarg(x_6, x_7);
x_9 = l_ML_EVar_getFresh(x_8);
lean_inc(x_9);
lean_inc(x_5);
lean_inc(x_4);
lean_inc(x_1);
x_10 = l_ML_ctxImplDefinedAux1___rarg(x_1, lean_box(0), x_3, x_4, x_5, x_9);
lean_inc(x_9);
x_11 = lean_alloc_ctor(0, 1, 0);
lean_ctor_set(x_11, 0, x_9);
lean_inc(x_4);
lean_inc(x_11);
x_12 = l_ML_Pattern_conjunction___rarg(x_11, x_4);
lean_inc(x_5);
x_13 = l_ML_AppContext_insert___rarg(x_12, x_5);
x_14 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_14, 0, x_1);
lean_inc(x_4);
x_15 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_15, 0, x_14);
lean_ctor_set(x_15, 1, x_4);
lean_inc(x_9);
lean_inc(x_15);
lean_inc(x_13);
x_16 = lean_alloc_ctor(4, 4, 0);
lean_ctor_set(x_16, 0, x_13);
lean_ctor_set(x_16, 1, x_15);
lean_ctor_set(x_16, 2, x_9);
lean_ctor_set(x_16, 3, x_10);
lean_inc(x_4);
x_17 = l_ML_Proof_implSelf___rarg(x_4);
lean_inc(x_9);
x_18 = lean_alloc_ctor(5, 1, 0);
lean_ctor_set(x_18, 0, x_9);
lean_inc(x_11);
lean_inc(x_9);
x_19 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_19, 0, x_9);
lean_ctor_set(x_19, 1, x_11);
lean_inc(x_4);
lean_inc(x_19);
x_20 = l_ML_Proof_extraPremise___rarg(x_19, x_4, x_18);
lean_inc(x_19);
lean_inc_n(x_4, 2);
x_21 = l_ML_Proof_conjIntroRule___rarg(x_4, x_19, x_4, x_20, x_17);
lean_inc(x_4);
x_22 = l_ML_Pattern_conjunction___rarg(x_19, x_4);
lean_inc(x_12);
lean_inc(x_9);
x_23 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_23, 0, x_9);
lean_ctor_set(x_23, 1, x_12);
lean_inc(x_9);
lean_inc(x_4);
x_24 = l_ML_Proof_pushConjInExist___rarg(x_11, x_4, x_9, lean_box(0));
lean_inc(x_23);
lean_inc(x_4);
x_25 = l_ML_Proof_syllogism___rarg(x_4, x_22, x_23, x_21, x_24);
lean_inc(x_5);
x_26 = l_ML_Proof_framing___rarg(x_4, x_23, x_5, x_25);
lean_dec(x_25);
lean_inc(x_5);
lean_inc(x_9);
x_27 = l_ML_Proof_propagationExist___rarg(x_12, x_9, x_5, lean_box(0));
lean_inc(x_5);
x_28 = l_ML_AppContext_insert___rarg(x_23, x_5);
lean_dec(x_23);
x_29 = lean_alloc_ctor(6, 2, 0);
lean_ctor_set(x_29, 0, x_9);
lean_ctor_set(x_29, 1, x_13);
lean_inc(x_15);
lean_inc(x_28);
x_30 = l_ML_Proof_syllogism___rarg(x_28, x_29, x_15, x_27, x_16);
x_31 = l_ML_AppContext_insert___rarg(x_4, x_5);
lean_dec(x_4);
x_32 = l_ML_Proof_syllogism___rarg(x_31, x_28, x_15, x_26, x_30);
return x_32;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefined(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_ctxImplDefined___rarg___boxed), 5, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_ctxImplDefined___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4, lean_object* x_5) {
_start:
{
lean_object* x_6; 
x_6 = l_ML_ctxImplDefined___rarg(x_1, x_2, x_3, x_4, x_5);
lean_dec(x_3);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_implDefined___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; 
x_5 = lean_box(0);
x_6 = l_ML_ctxImplDefined___rarg(x_1, lean_box(0), x_3, x_4, x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_ML_implDefined(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_implDefined___rarg___boxed), 4, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_implDefined___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_implDefined___rarg(x_1, x_2, x_3, x_4);
lean_dec(x_3);
return x_5;
}
}
LEAN_EXPORT lean_object* l_ML_totalImpl___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; lean_object* x_6; lean_object* x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; 
lean_inc(x_4);
x_5 = l_ML_Pattern_negation___rarg(x_4);
lean_inc(x_5);
lean_inc(x_1);
x_6 = l_ML_implDefined___rarg(x_1, lean_box(0), x_3, x_5);
lean_inc(x_1);
x_7 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_7, 0, x_1);
lean_inc(x_5);
x_8 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_8, 0, x_7);
lean_ctor_set(x_8, 1, x_5);
lean_inc(x_5);
x_9 = l_ML_Proof_negImplIntro___rarg(x_5, x_8, x_6);
lean_inc(x_4);
x_10 = l_ML_total___rarg(x_1, x_4);
x_11 = l_ML_Pattern_negation___rarg(x_5);
lean_inc(x_4);
x_12 = l_ML_Proof_doubleNegElim___rarg(x_4);
x_13 = l_ML_Proof_syllogism___rarg(x_10, x_11, x_4, x_9, x_12);
return x_13;
}
}
LEAN_EXPORT lean_object* l_ML_totalImpl(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l_ML_totalImpl___rarg___boxed), 4, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l_ML_totalImpl___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3, lean_object* x_4) {
_start:
{
lean_object* x_5; 
x_5 = l_ML_totalImpl___rarg(x_1, x_2, x_3, x_4);
lean_dec(x_3);
return x_5;
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Pattern(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Proof(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_MatchingLogic_Definedness(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Pattern(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Proof(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
l_ML_instHasDefinedDefinedness = _init_l_ML_instHasDefinedDefinedness();
lean_mark_persistent(l_ML_instHasDefinedDefinedness);
l_ML_term_u2308___u2309___closed__1 = _init_l_ML_term_u2308___u2309___closed__1();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__1);
l_ML_term_u2308___u2309___closed__2 = _init_l_ML_term_u2308___u2309___closed__2();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__2);
l_ML_term_u2308___u2309___closed__3 = _init_l_ML_term_u2308___u2309___closed__3();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__3);
l_ML_term_u2308___u2309___closed__4 = _init_l_ML_term_u2308___u2309___closed__4();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__4);
l_ML_term_u2308___u2309___closed__5 = _init_l_ML_term_u2308___u2309___closed__5();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__5);
l_ML_term_u2308___u2309___closed__6 = _init_l_ML_term_u2308___u2309___closed__6();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__6);
l_ML_term_u2308___u2309___closed__7 = _init_l_ML_term_u2308___u2309___closed__7();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__7);
l_ML_term_u2308___u2309___closed__8 = _init_l_ML_term_u2308___u2309___closed__8();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__8);
l_ML_term_u2308___u2309___closed__9 = _init_l_ML_term_u2308___u2309___closed__9();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__9);
l_ML_term_u2308___u2309___closed__10 = _init_l_ML_term_u2308___u2309___closed__10();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__10);
l_ML_term_u2308___u2309___closed__11 = _init_l_ML_term_u2308___u2309___closed__11();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__11);
l_ML_term_u2308___u2309___closed__12 = _init_l_ML_term_u2308___u2309___closed__12();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__12);
l_ML_term_u2308___u2309___closed__13 = _init_l_ML_term_u2308___u2309___closed__13();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__13);
l_ML_term_u2308___u2309___closed__14 = _init_l_ML_term_u2308___u2309___closed__14();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__14);
l_ML_term_u2308___u2309___closed__15 = _init_l_ML_term_u2308___u2309___closed__15();
lean_mark_persistent(l_ML_term_u2308___u2309___closed__15);
l_ML_term_u2308___u2309 = _init_l_ML_term_u2308___u2309();
lean_mark_persistent(l_ML_term_u2308___u2309);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__8);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__9);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__10);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__11);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__12);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__13);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__14);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__15);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__16);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__17);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__18);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__19);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__20);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__21);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__22);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__23);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__24);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__25);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__26);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__27);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__28);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__29);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__30);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__31);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u2308___u2309__1___closed__32);
l_ML_term_u230a___u230b___closed__1 = _init_l_ML_term_u230a___u230b___closed__1();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__1);
l_ML_term_u230a___u230b___closed__2 = _init_l_ML_term_u230a___u230b___closed__2();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__2);
l_ML_term_u230a___u230b___closed__3 = _init_l_ML_term_u230a___u230b___closed__3();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__3);
l_ML_term_u230a___u230b___closed__4 = _init_l_ML_term_u230a___u230b___closed__4();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__4);
l_ML_term_u230a___u230b___closed__5 = _init_l_ML_term_u230a___u230b___closed__5();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__5);
l_ML_term_u230a___u230b___closed__6 = _init_l_ML_term_u230a___u230b___closed__6();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__6);
l_ML_term_u230a___u230b___closed__7 = _init_l_ML_term_u230a___u230b___closed__7();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__7);
l_ML_term_u230a___u230b___closed__8 = _init_l_ML_term_u230a___u230b___closed__8();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__8);
l_ML_term_u230a___u230b___closed__9 = _init_l_ML_term_u230a___u230b___closed__9();
lean_mark_persistent(l_ML_term_u230a___u230b___closed__9);
l_ML_term_u230a___u230b = _init_l_ML_term_u230a___u230b();
lean_mark_persistent(l_ML_term_u230a___u230b);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term_u230a___u230b__1___closed__8);
l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__1);
l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______unexpand__ML__total__1___closed__2);
l_ML_term___u2261_____closed__1 = _init_l_ML_term___u2261_____closed__1();
lean_mark_persistent(l_ML_term___u2261_____closed__1);
l_ML_term___u2261_____closed__2 = _init_l_ML_term___u2261_____closed__2();
lean_mark_persistent(l_ML_term___u2261_____closed__2);
l_ML_term___u2261_____closed__3 = _init_l_ML_term___u2261_____closed__3();
lean_mark_persistent(l_ML_term___u2261_____closed__3);
l_ML_term___u2261_____closed__4 = _init_l_ML_term___u2261_____closed__4();
lean_mark_persistent(l_ML_term___u2261_____closed__4);
l_ML_term___u2261_____closed__5 = _init_l_ML_term___u2261_____closed__5();
lean_mark_persistent(l_ML_term___u2261_____closed__5);
l_ML_term___u2261_____closed__6 = _init_l_ML_term___u2261_____closed__6();
lean_mark_persistent(l_ML_term___u2261_____closed__6);
l_ML_term___u2261_____closed__7 = _init_l_ML_term___u2261_____closed__7();
lean_mark_persistent(l_ML_term___u2261_____closed__7);
l_ML_term___u2261__ = _init_l_ML_term___u2261__();
lean_mark_persistent(l_ML_term___u2261__);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2261____1___closed__8);
l_ML_term___u2208_u2208_____closed__1 = _init_l_ML_term___u2208_u2208_____closed__1();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__1);
l_ML_term___u2208_u2208_____closed__2 = _init_l_ML_term___u2208_u2208_____closed__2();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__2);
l_ML_term___u2208_u2208_____closed__3 = _init_l_ML_term___u2208_u2208_____closed__3();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__3);
l_ML_term___u2208_u2208_____closed__4 = _init_l_ML_term___u2208_u2208_____closed__4();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__4);
l_ML_term___u2208_u2208_____closed__5 = _init_l_ML_term___u2208_u2208_____closed__5();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__5);
l_ML_term___u2208_u2208_____closed__6 = _init_l_ML_term___u2208_u2208_____closed__6();
lean_mark_persistent(l_ML_term___u2208_u2208_____closed__6);
l_ML_term___u2208_u2208__ = _init_l_ML_term___u2208_u2208__();
lean_mark_persistent(l_ML_term___u2208_u2208__);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2208_u2208____1___closed__8);
l_ML_term___u2286_u2286_____closed__1 = _init_l_ML_term___u2286_u2286_____closed__1();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__1);
l_ML_term___u2286_u2286_____closed__2 = _init_l_ML_term___u2286_u2286_____closed__2();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__2);
l_ML_term___u2286_u2286_____closed__3 = _init_l_ML_term___u2286_u2286_____closed__3();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__3);
l_ML_term___u2286_u2286_____closed__4 = _init_l_ML_term___u2286_u2286_____closed__4();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__4);
l_ML_term___u2286_u2286_____closed__5 = _init_l_ML_term___u2286_u2286_____closed__5();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__5);
l_ML_term___u2286_u2286_____closed__6 = _init_l_ML_term___u2286_u2286_____closed__6();
lean_mark_persistent(l_ML_term___u2286_u2286_____closed__6);
l_ML_term___u2286_u2286__ = _init_l_ML_term___u2286_u2286__();
lean_mark_persistent(l_ML_term___u2286_u2286__);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__1);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__2);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__3);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__4);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__5);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__6);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__7);
l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8 = _init_l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8();
lean_mark_persistent(l_ML___aux__MatchingLogic__Definedness______macroRules__ML__term___u2286_u2286____1___closed__8);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__1 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__1();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__1);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__2 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__2();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__2);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__3 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__3();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__3);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__4 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__4();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__4);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__5 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__5();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__5);
l_ML_isApplicationOfDefinedSymbolToPattern___closed__6 = _init_l_ML_isApplicationOfDefinedSymbolToPattern___closed__6();
lean_mark_persistent(l_ML_isApplicationOfDefinedSymbolToPattern___closed__6);
l_ML_delabDefined___closed__1 = _init_l_ML_delabDefined___closed__1();
lean_mark_persistent(l_ML_delabDefined___closed__1);
l_ML_delabDefined___closed__2 = _init_l_ML_delabDefined___closed__2();
lean_mark_persistent(l_ML_delabDefined___closed__2);
l_ML_delabDefined___closed__3 = _init_l_ML_delabDefined___closed__3();
lean_mark_persistent(l_ML_delabDefined___closed__3);
l_ML_delabDefined___closed__4 = _init_l_ML_delabDefined___closed__4();
lean_mark_persistent(l_ML_delabDefined___closed__4);
l_ML_delabDefined___closed__5 = _init_l_ML_delabDefined___closed__5();
lean_mark_persistent(l_ML_delabDefined___closed__5);
l_ML_delabDefined___closed__6 = _init_l_ML_delabDefined___closed__6();
lean_mark_persistent(l_ML_delabDefined___closed__6);
l_ML_delabDefined___closed__7 = _init_l_ML_delabDefined___closed__7();
lean_mark_persistent(l_ML_delabDefined___closed__7);
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif

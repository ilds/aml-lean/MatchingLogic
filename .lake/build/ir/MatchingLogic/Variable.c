// Lean compiler output
// Module: MatchingLogic.Variable
// Imports: Init MatchingLogic.Util
#include <lean/lean.h>
#if defined(__clang__)
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-label"
#elif defined(__GNUC__) && !defined(__CLANG__)
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-label"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#endif
#ifdef __cplusplus
extern "C" {
#endif
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_reprSVar____x40_MatchingLogic_Variable___hyg_797____boxed(lean_object*, lean_object*);
static lean_object* l_ML_EVar_instToString___closed__1;
LEAN_EXPORT lean_object* l_ML_instReprSVar;
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__9;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_getFresh_match__1_splitter(lean_object*);
LEAN_EXPORT lean_object* l_ML_instDecidableEqSVar___boxed(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_List_mapTR_loop___at_ML_EVar_getFresh___spec__1(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_instInhabitedEVar;
LEAN_EXPORT lean_object* l_ML_EVar_instToString(lean_object*);
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__8;
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__3;
LEAN_EXPORT lean_object* l_ML_instReprEVar;
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__11;
static lean_object* l_ML_SVar_instToString___closed__1;
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__10;
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__12;
LEAN_EXPORT lean_object* l_ML_SVar_instToString(lean_object*);
lean_object* lean_nat_to_int(lean_object*);
static lean_object* l_ML_instReprEVar___closed__1;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____boxed(lean_object*, lean_object*);
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__7;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_decEqSVar____x40_MatchingLogic_Variable___hyg_696____boxed(lean_object*, lean_object*);
LEAN_EXPORT uint8_t l___private_MatchingLogic_Variable_0__ML_decEqEVar____x40_MatchingLogic_Variable___hyg_307_(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_getFresh___boxed(lean_object*);
LEAN_EXPORT uint8_t l___private_MatchingLogic_Variable_0__ML_decEqSVar____x40_MatchingLogic_Variable___hyg_696_(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_getFresh(lean_object*);
LEAN_EXPORT lean_object* l_ML_EVar_getFresh(lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408_(lean_object*, lean_object*);
static lean_object* l_ML_EVar_instToString___closed__2;
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__6;
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__13;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_getFresh_match__1_splitter___rarg___boxed(lean_object*, lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_getFresh_match__1_splitter___rarg(lean_object*, lean_object*, lean_object*);
lean_object* lean_string_length(lean_object*);
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__1;
uint8_t lean_nat_dec_eq(lean_object*, lean_object*);
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__4;
LEAN_EXPORT lean_object* l_ML_instDecidableEqEVar___boxed(lean_object*, lean_object*);
LEAN_EXPORT uint8_t l_ML_instDecidableEqSVar(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_SVar_getFresh(lean_object*);
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__2;
lean_object* l_List_reverse___rarg(lean_object*);
lean_object* lean_string_append(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_ML_instInhabitedSVar;
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_reprSVar____x40_MatchingLogic_Variable___hyg_797_(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l_List_mapTR_loop___at_ML_SVar_getFresh___spec__1(lean_object*, lean_object*);
static lean_object* l_ML_instReprSVar___closed__1;
uint8_t lean_nat_dec_le(lean_object*, lean_object*);
lean_object* lean_nat_add(lean_object*, lean_object*);
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_decEqEVar____x40_MatchingLogic_Variable___hyg_307____boxed(lean_object*, lean_object*);
lean_object* l___private_Init_Data_Repr_0__Nat_reprFast(lean_object*);
LEAN_EXPORT uint8_t l_ML_instDecidableEqEVar(lean_object*, lean_object*);
static lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__5;
LEAN_EXPORT lean_object* l_ML_getFresh(lean_object* x_1) {
_start:
{
if (lean_obj_tag(x_1) == 0)
{
lean_object* x_2; 
x_2 = lean_unsigned_to_nat(0u);
return x_2;
}
else
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; uint8_t x_6; 
x_3 = lean_ctor_get(x_1, 0);
x_4 = lean_ctor_get(x_1, 1);
x_5 = l_ML_getFresh(x_4);
x_6 = lean_nat_dec_le(x_3, x_5);
if (x_6 == 0)
{
lean_object* x_7; lean_object* x_8; 
lean_dec(x_5);
x_7 = lean_unsigned_to_nat(1u);
x_8 = lean_nat_add(x_3, x_7);
return x_8;
}
else
{
lean_object* x_9; lean_object* x_10; 
x_9 = lean_unsigned_to_nat(1u);
x_10 = lean_nat_add(x_5, x_9);
lean_dec(x_5);
return x_10;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_getFresh___boxed(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = l_ML_getFresh(x_1);
lean_dec(x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_getFresh_match__1_splitter___rarg(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
if (lean_obj_tag(x_1) == 0)
{
lean_dec(x_3);
lean_inc(x_2);
return x_2;
}
else
{
lean_object* x_4; lean_object* x_5; lean_object* x_6; 
x_4 = lean_ctor_get(x_1, 0);
lean_inc(x_4);
x_5 = lean_ctor_get(x_1, 1);
lean_inc(x_5);
lean_dec(x_1);
x_6 = lean_apply_2(x_3, x_4, x_5);
return x_6;
}
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_getFresh_match__1_splitter(lean_object* x_1) {
_start:
{
lean_object* x_2; 
x_2 = lean_alloc_closure((void*)(l___private_MatchingLogic_Variable_0__ML_getFresh_match__1_splitter___rarg___boxed), 3, 0);
return x_2;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_getFresh_match__1_splitter___rarg___boxed(lean_object* x_1, lean_object* x_2, lean_object* x_3) {
_start:
{
lean_object* x_4; 
x_4 = l___private_MatchingLogic_Variable_0__ML_getFresh_match__1_splitter___rarg(x_1, x_2, x_3);
lean_dec(x_2);
return x_4;
}
}
LEAN_EXPORT uint8_t l___private_MatchingLogic_Variable_0__ML_decEqEVar____x40_MatchingLogic_Variable___hyg_307_(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; 
x_3 = lean_nat_dec_eq(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_decEqEVar____x40_MatchingLogic_Variable___hyg_307____boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l___private_MatchingLogic_Variable_0__ML_decEqEVar____x40_MatchingLogic_Variable___hyg_307_(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
LEAN_EXPORT uint8_t l_ML_instDecidableEqEVar(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; 
x_3 = lean_nat_dec_eq(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_instDecidableEqEVar___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l_ML_instDecidableEqEVar(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
static lean_object* _init_l_ML_instInhabitedEVar() {
_start:
{
lean_object* x_1; 
x_1 = lean_unsigned_to_nat(0u);
return x_1;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("val", 3, 3);
return x_1;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__2() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__1;
x_2 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__3() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = lean_box(0);
x_2 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__2;
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__4() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked(" := ", 4, 4);
return x_1;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__5() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__4;
x_2 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__6() {
_start:
{
lean_object* x_1; lean_object* x_2; lean_object* x_3; 
x_1 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__3;
x_2 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__5;
x_3 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_3, 0, x_1);
lean_ctor_set(x_3, 1, x_2);
return x_3;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__7() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = lean_unsigned_to_nat(7u);
x_2 = lean_nat_to_int(x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__8() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("{ ", 2, 2);
return x_1;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__9() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__8;
x_2 = lean_string_length(x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__10() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__9;
x_2 = lean_nat_to_int(x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__11() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__8;
x_2 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__12() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked(" }", 2, 2);
return x_1;
}
}
static lean_object* _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__13() {
_start:
{
lean_object* x_1; lean_object* x_2; 
x_1 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__12;
x_2 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_2, 0, x_1);
return x_2;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408_(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; uint8_t x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; 
x_3 = l___private_Init_Data_Repr_0__Nat_reprFast(x_1);
x_4 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_4, 0, x_3);
x_5 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__7;
x_6 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_6, 0, x_5);
lean_ctor_set(x_6, 1, x_4);
x_7 = 0;
x_8 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_8, 0, x_6);
lean_ctor_set_uint8(x_8, sizeof(void*)*1, x_7);
x_9 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__6;
x_10 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_10, 0, x_9);
lean_ctor_set(x_10, 1, x_8);
x_11 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__11;
x_12 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_12, 0, x_11);
lean_ctor_set(x_12, 1, x_10);
x_13 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__13;
x_14 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_14, 0, x_12);
lean_ctor_set(x_14, 1, x_13);
x_15 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__10;
x_16 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_16, 0, x_15);
lean_ctor_set(x_16, 1, x_14);
x_17 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_17, 0, x_16);
lean_ctor_set_uint8(x_17, sizeof(void*)*1, x_7);
return x_17;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408_(x_1, x_2);
lean_dec(x_2);
return x_3;
}
}
static lean_object* _init_l_ML_instReprEVar___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_alloc_closure((void*)(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____boxed), 2, 0);
return x_1;
}
}
static lean_object* _init_l_ML_instReprEVar() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_instReprEVar___closed__1;
return x_1;
}
}
static lean_object* _init_l_ML_EVar_instToString___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("x", 1, 1);
return x_1;
}
}
static lean_object* _init_l_ML_EVar_instToString___closed__2() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("", 0, 0);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML_EVar_instToString(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
x_2 = l___private_Init_Data_Repr_0__Nat_reprFast(x_1);
x_3 = l_ML_EVar_instToString___closed__1;
x_4 = lean_string_append(x_3, x_2);
lean_dec(x_2);
x_5 = l_ML_EVar_instToString___closed__2;
x_6 = lean_string_append(x_4, x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_List_mapTR_loop___at_ML_EVar_getFresh___spec__1(lean_object* x_1, lean_object* x_2) {
_start:
{
if (lean_obj_tag(x_1) == 0)
{
lean_object* x_3; 
x_3 = l_List_reverse___rarg(x_2);
return x_3;
}
else
{
uint8_t x_4; 
x_4 = !lean_is_exclusive(x_1);
if (x_4 == 0)
{
lean_object* x_5; 
x_5 = lean_ctor_get(x_1, 1);
lean_ctor_set(x_1, 1, x_2);
{
lean_object* _tmp_0 = x_5;
lean_object* _tmp_1 = x_1;
x_1 = _tmp_0;
x_2 = _tmp_1;
}
goto _start;
}
else
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; 
x_7 = lean_ctor_get(x_1, 0);
x_8 = lean_ctor_get(x_1, 1);
lean_inc(x_8);
lean_inc(x_7);
lean_dec(x_1);
x_9 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_9, 0, x_7);
lean_ctor_set(x_9, 1, x_2);
x_1 = x_8;
x_2 = x_9;
goto _start;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_EVar_getFresh(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_2 = lean_box(0);
x_3 = l_List_mapTR_loop___at_ML_EVar_getFresh___spec__1(x_1, x_2);
x_4 = l_ML_getFresh(x_3);
lean_dec(x_3);
return x_4;
}
}
LEAN_EXPORT uint8_t l___private_MatchingLogic_Variable_0__ML_decEqSVar____x40_MatchingLogic_Variable___hyg_696_(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; 
x_3 = lean_nat_dec_eq(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_decEqSVar____x40_MatchingLogic_Variable___hyg_696____boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l___private_MatchingLogic_Variable_0__ML_decEqSVar____x40_MatchingLogic_Variable___hyg_696_(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
LEAN_EXPORT uint8_t l_ML_instDecidableEqSVar(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; 
x_3 = lean_nat_dec_eq(x_1, x_2);
return x_3;
}
}
LEAN_EXPORT lean_object* l_ML_instDecidableEqSVar___boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
uint8_t x_3; lean_object* x_4; 
x_3 = l_ML_instDecidableEqSVar(x_1, x_2);
lean_dec(x_2);
lean_dec(x_1);
x_4 = lean_box(x_3);
return x_4;
}
}
static lean_object* _init_l_ML_instInhabitedSVar() {
_start:
{
lean_object* x_1; 
x_1 = lean_unsigned_to_nat(0u);
return x_1;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_reprSVar____x40_MatchingLogic_Variable___hyg_797_(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; uint8_t x_7; lean_object* x_8; lean_object* x_9; lean_object* x_10; lean_object* x_11; lean_object* x_12; lean_object* x_13; lean_object* x_14; lean_object* x_15; lean_object* x_16; lean_object* x_17; 
x_3 = l___private_Init_Data_Repr_0__Nat_reprFast(x_1);
x_4 = lean_alloc_ctor(3, 1, 0);
lean_ctor_set(x_4, 0, x_3);
x_5 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__7;
x_6 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_6, 0, x_5);
lean_ctor_set(x_6, 1, x_4);
x_7 = 0;
x_8 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_8, 0, x_6);
lean_ctor_set_uint8(x_8, sizeof(void*)*1, x_7);
x_9 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__6;
x_10 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_10, 0, x_9);
lean_ctor_set(x_10, 1, x_8);
x_11 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__11;
x_12 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_12, 0, x_11);
lean_ctor_set(x_12, 1, x_10);
x_13 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__13;
x_14 = lean_alloc_ctor(5, 2, 0);
lean_ctor_set(x_14, 0, x_12);
lean_ctor_set(x_14, 1, x_13);
x_15 = l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__10;
x_16 = lean_alloc_ctor(4, 2, 0);
lean_ctor_set(x_16, 0, x_15);
lean_ctor_set(x_16, 1, x_14);
x_17 = lean_alloc_ctor(6, 1, 1);
lean_ctor_set(x_17, 0, x_16);
lean_ctor_set_uint8(x_17, sizeof(void*)*1, x_7);
return x_17;
}
}
LEAN_EXPORT lean_object* l___private_MatchingLogic_Variable_0__ML_reprSVar____x40_MatchingLogic_Variable___hyg_797____boxed(lean_object* x_1, lean_object* x_2) {
_start:
{
lean_object* x_3; 
x_3 = l___private_MatchingLogic_Variable_0__ML_reprSVar____x40_MatchingLogic_Variable___hyg_797_(x_1, x_2);
lean_dec(x_2);
return x_3;
}
}
static lean_object* _init_l_ML_instReprSVar___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_alloc_closure((void*)(l___private_MatchingLogic_Variable_0__ML_reprSVar____x40_MatchingLogic_Variable___hyg_797____boxed), 2, 0);
return x_1;
}
}
static lean_object* _init_l_ML_instReprSVar() {
_start:
{
lean_object* x_1; 
x_1 = l_ML_instReprSVar___closed__1;
return x_1;
}
}
static lean_object* _init_l_ML_SVar_instToString___closed__1() {
_start:
{
lean_object* x_1; 
x_1 = lean_mk_string_unchecked("X", 1, 1);
return x_1;
}
}
LEAN_EXPORT lean_object* l_ML_SVar_instToString(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; lean_object* x_5; lean_object* x_6; 
x_2 = l___private_Init_Data_Repr_0__Nat_reprFast(x_1);
x_3 = l_ML_SVar_instToString___closed__1;
x_4 = lean_string_append(x_3, x_2);
lean_dec(x_2);
x_5 = l_ML_EVar_instToString___closed__2;
x_6 = lean_string_append(x_4, x_5);
return x_6;
}
}
LEAN_EXPORT lean_object* l_List_mapTR_loop___at_ML_SVar_getFresh___spec__1(lean_object* x_1, lean_object* x_2) {
_start:
{
if (lean_obj_tag(x_1) == 0)
{
lean_object* x_3; 
x_3 = l_List_reverse___rarg(x_2);
return x_3;
}
else
{
uint8_t x_4; 
x_4 = !lean_is_exclusive(x_1);
if (x_4 == 0)
{
lean_object* x_5; 
x_5 = lean_ctor_get(x_1, 1);
lean_ctor_set(x_1, 1, x_2);
{
lean_object* _tmp_0 = x_5;
lean_object* _tmp_1 = x_1;
x_1 = _tmp_0;
x_2 = _tmp_1;
}
goto _start;
}
else
{
lean_object* x_7; lean_object* x_8; lean_object* x_9; 
x_7 = lean_ctor_get(x_1, 0);
x_8 = lean_ctor_get(x_1, 1);
lean_inc(x_8);
lean_inc(x_7);
lean_dec(x_1);
x_9 = lean_alloc_ctor(1, 2, 0);
lean_ctor_set(x_9, 0, x_7);
lean_ctor_set(x_9, 1, x_2);
x_1 = x_8;
x_2 = x_9;
goto _start;
}
}
}
}
LEAN_EXPORT lean_object* l_ML_SVar_getFresh(lean_object* x_1) {
_start:
{
lean_object* x_2; lean_object* x_3; lean_object* x_4; 
x_2 = lean_box(0);
x_3 = l_List_mapTR_loop___at_ML_SVar_getFresh___spec__1(x_1, x_2);
x_4 = l_ML_getFresh(x_3);
lean_dec(x_3);
return x_4;
}
}
lean_object* initialize_Init(uint8_t builtin, lean_object*);
lean_object* initialize_MatchingLogic_Util(uint8_t builtin, lean_object*);
static bool _G_initialized = false;
LEAN_EXPORT lean_object* initialize_MatchingLogic_Variable(uint8_t builtin, lean_object* w) {
lean_object * res;
if (_G_initialized) return lean_io_result_mk_ok(lean_box(0));
_G_initialized = true;
res = initialize_Init(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
res = initialize_MatchingLogic_Util(builtin, lean_io_mk_world());
if (lean_io_result_is_error(res)) return res;
lean_dec_ref(res);
l_ML_instInhabitedEVar = _init_l_ML_instInhabitedEVar();
lean_mark_persistent(l_ML_instInhabitedEVar);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__1 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__1();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__1);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__2 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__2();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__2);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__3 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__3();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__3);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__4 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__4();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__4);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__5 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__5();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__5);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__6 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__6();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__6);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__7 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__7();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__7);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__8 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__8();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__8);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__9 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__9();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__9);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__10 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__10();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__10);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__11 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__11();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__11);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__12 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__12();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__12);
l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__13 = _init_l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__13();
lean_mark_persistent(l___private_MatchingLogic_Variable_0__ML_reprEVar____x40_MatchingLogic_Variable___hyg_408____closed__13);
l_ML_instReprEVar___closed__1 = _init_l_ML_instReprEVar___closed__1();
lean_mark_persistent(l_ML_instReprEVar___closed__1);
l_ML_instReprEVar = _init_l_ML_instReprEVar();
lean_mark_persistent(l_ML_instReprEVar);
l_ML_EVar_instToString___closed__1 = _init_l_ML_EVar_instToString___closed__1();
lean_mark_persistent(l_ML_EVar_instToString___closed__1);
l_ML_EVar_instToString___closed__2 = _init_l_ML_EVar_instToString___closed__2();
lean_mark_persistent(l_ML_EVar_instToString___closed__2);
l_ML_instInhabitedSVar = _init_l_ML_instInhabitedSVar();
lean_mark_persistent(l_ML_instInhabitedSVar);
l_ML_instReprSVar___closed__1 = _init_l_ML_instReprSVar___closed__1();
lean_mark_persistent(l_ML_instReprSVar___closed__1);
l_ML_instReprSVar = _init_l_ML_instReprSVar();
lean_mark_persistent(l_ML_instReprSVar);
l_ML_SVar_instToString___closed__1 = _init_l_ML_SVar_instToString___closed__1();
lean_mark_persistent(l_ML_SVar_instToString___closed__1);
return lean_io_result_mk_ok(lean_box(0));
}
#ifdef __cplusplus
}
#endif

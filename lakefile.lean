import Lake

open Lake DSL

package MatchingLogic

@[default_target]
lean_lib MatchingLogic

require mathlib from git
  "https://github.com/leanprover-community/mathlib4.git"

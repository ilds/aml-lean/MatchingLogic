import MatchingLogic.Pattern
import MatchingLogic.Proof

namespace ML


example (x y : ℕ) : (x + y) = 1 → (x = 1 ∧ y = 0) ⊕' (y = 1 ∧ x = 0) := by
  intros h
  cases x with
  | zero =>
    apply PSum.inr
    omega
  | succ x' =>
    cases y with
    | zero =>
      apply PSum.inl
      omega
    | succ y' =>
      omega

inductive SubstContext (𝕊 : Type) : Type where
| box : SubstContext 𝕊
| applicationLeft : Pattern 𝕊 → SubstContext 𝕊 → SubstContext 𝕊
| applicationRight : SubstContext 𝕊 → Pattern 𝕊 → SubstContext 𝕊
| implicationLeft : Pattern 𝕊 → SubstContext 𝕊 → SubstContext 𝕊
| implicationRight : SubstContext 𝕊 → Pattern 𝕊 → SubstContext 𝕊
| existential : EVar → SubstContext 𝕊 → SubstContext 𝕊
| mu : SVar → SubstContext 𝕊 → SubstContext 𝕊

@[simp]
def SubstContext.insert {𝕊} (φ : Pattern 𝕊) (C : SubstContext 𝕊) :=
  match C with
  | box => φ
  | applicationLeft ψ C' => ψ ⬝ (C'.insert φ)
  | applicationRight C' ψ => (C'.insert φ) ⬝ ψ
  | implicationLeft ψ C' => ψ ⇒ (C'.insert φ)
  | implicationRight C' ψ => (C'.insert φ) ⇒ ψ
  | existential x C' => ∃∃ x (C'.insert φ)
  | mu X C' => μ X (C'.insert φ)

@[simp]
def SubstContext.muFree {𝕊} (C : SubstContext 𝕊) : Bool :=
  match C with
  | mu _ _ => false
  | box => true
  | applicationLeft _ C' => C'.muFree
  | applicationRight C' _ => C'.muFree
  | implicationLeft _ C' => C'.muFree
  | implicationRight C' _ => C'.muFree
  | existential _ C' => C'.muFree

-- TODO: refactor in `match by`
def SubstContext.preservesEquivMuFree {𝕊} (Γ : Premises 𝕊) (φ ψ : Pattern 𝕊) (C : SubstContext 𝕊) (hμ : C.muFree) :
  Γ ⊢ φ ⇔ ψ → Γ ⊢ C.insert φ ⇔ C.insert ψ := by
  intros h
  induction C with
  | box =>
    simp
    exact h
  | applicationLeft χ C' ih =>
    simp at hμ ⊢
    exact Proof.framingEquiv (C := AppContext.empty.left χ) (ih hμ)
  | applicationRight C' χ ih =>
    simp at hμ ⊢
    exact Proof.framingEquiv (C := AppContext.empty.right χ) (ih hμ)
  | implicationLeft χ C' ih =>
    simp at hμ ⊢
    exact Proof.implPreservesEquivLeft.toRule (ih hμ)
  | implicationRight C' χ ih =>
    simp at hμ ⊢
    exact Proof.implPreservesEquivRight.toRule (ih hμ)
  | existential x C' ih =>
    simp at hμ ⊢
    exact Proof.existPreservesEquiv (ih hμ)
  | mu X C' ih =>
    simp at hμ

-- TODO: refactor in `match by`
def SubstContext.preservesEquiv {𝕊} (Γ : Premises 𝕊) (φ ψ : Pattern 𝕊) (C : SubstContext 𝕊)  :
  Γ ⊢ φ ⇔ ψ → Γ ⊢ C.insert φ ⇔ C.insert ψ := by
  intros h
  induction C with
  | box =>
    simp
    exact h
  | applicationLeft χ C' ih =>
    simp
    exact Proof.framingEquiv (C := AppContext.empty.left χ) ih
  | applicationRight C' χ ih =>
    simp
    exact Proof.framingEquiv (C := AppContext.empty.right χ) ih
  | implicationLeft χ C' ih =>
    simp
    exact Proof.implPreservesEquivLeft.toRule ih
  | implicationRight C' χ ih =>
    simp
    exact Proof.implPreservesEquivRight.toRule ih
  | existential x C' ih =>
    simp
    exact Proof.existPreservesEquiv ih
  | mu X C' ih =>
    simp
    let ih₁ := Proof.conjElimLeft.toRule ih
    let ih₂ := Proof.conjElimRight.toRule ih
    -- needs a positivity assumption here
    exact Proof.conjIntroRule' (Proof.muMonotone sorry ih₁) (Proof.muMonotone sorry ih₂)

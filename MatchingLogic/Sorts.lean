import MatchingLogic.Definedness

namespace ML

set_option autoImplicit false

class HasSorts (𝕊 : Type) extends HasDefined 𝕊 where
  sortUniverse : 𝕊
  inhabitants : 𝕊

variable {𝕊 : Type} [HasSorts 𝕊]

def Pattern.inhabitants (s : Pattern 𝕊) : Pattern 𝕊 :=
  (.symbol HasSorts.inhabitants) ⬝ s

notation "⟦" s "⟧" => Pattern.inhabitants s

def negationSorted (s φ : Pattern 𝕊) : Pattern 𝕊 :=
  ∼φ ⋀ ⟦s⟧

def universalSorted (s : Pattern 𝕊) (x : EVar) (φ : Pattern 𝕊) : Pattern 𝕊 :=
  ∀∀ x ((x ∈∈ ⟦s⟧) ⇒ φ)

def existentialSorted (s : Pattern 𝕊) (x : EVar) (φ : Pattern 𝕊) : Pattern 𝕊 :=
  ∃∃ x ((x ∈∈ ⟦s⟧) ⋀ φ)

def subsort (s₁ s₂ : Pattern 𝕊) : Pattern 𝕊 :=
  ⟦s₁⟧ ⊆⊆ ⟦s₂⟧

def ofSort (φ : Pattern 𝕊) (s : Pattern 𝕊) : Pattern 𝕊 :=
  φ ⊆⊆ ⟦s⟧



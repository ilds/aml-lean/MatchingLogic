import MatchingLogic.Pattern 

set_option autoImplicit false 

namespace ML.Sorted 

variable {𝕊 : Type} {𝒮 : Type} [DecidableEq 𝒮]

inductive SortSystem (𝒮 : Type) where 
  | base : 𝒮 → SortSystem 𝒮 
  | arrow : SortSystem 𝒮 → SortSystem 𝒮 → SortSystem 𝒮 
  deriving DecidableEq

def implicationSort (s₁ s₂ : (SortSystem 𝒮)) : Option (SortSystem 𝒮) := 
  if s₁ = s₂ then some s₁ else none 

def applicationSort (s₁ s₂ : (SortSystem 𝒮)) : Option (SortSystem 𝒮) := 
  match s₁ with 
  | .arrow s₁' s₁'' => 
    if s₁' = s₂ then s₁'' else none 
  | _ => none 

def inferSort (𝒮 : Type) [DecidableEq 𝒮] (φ : Pattern 𝕊) : Option (SortSystem 𝒮) := do 
  match φ with 
  | φ₁ ⇒ φ₂ => implicationSort (← inferSort 𝒮 φ₁) (← inferSort 𝒮 φ₂)
  | φ₁ ⬝ φ₂ => applicationSort (← inferSort 𝒮 φ₁) (← inferSort 𝒮 φ₂)
  | ∃∃ x φ => inferSort 𝒮 φ
  | μ X φ => inferSort 𝒮 φ  
  | .symbol σ => _